/*jslint */
/*global AdobeEdge: false, window: false, document: false, console:false, alert: false */
(function (compId) {

    "use strict";
    var im='images/',
        aud='media/',
        vid='media/',
        js='js/',
        fonts = {
            'arimo, sans-serif': '<script src=\"http://use.edgefonts.net/arimo:n4,i4,n7,i7:all.js\"></script>'        },
        opts = {
            'gAudioPreloadPreference': 'auto',
            'gVideoPreloadPreference': 'auto'
        },
        resources = [
        ],
        scripts = [
        ],
        symbols = {
            "stage": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: 'sStandupPic',
                            symbolName: 'sStandupPic',
                            type: 'rect',
                            rect: ['0', '0', '1200', '608', 'auto', 'auto'],
                            opacity: '1'
                        },
                        {
                            id: 'Heading',
                            type: 'text',
                            rect: ['50px', '224px', '340px', '40px', 'auto', 'auto'],
                            text: "<p style=\"margin: 0px;\">​Web development</p><p style=\"margin: 0px;\">Web development can be simply defined as the task of creating websites.</p>",
                            userClass: "text-h1 black",
                            font: ['arimo, sans-serif', [35, "px"], "rgba(27,83,120,1.00)", "normal", "none", "", "break-word", "normal"],
                            textStyle: ["", "", "35px", "", ""]
                        },
                        {
                            id: 'sIcon',
                            symbolName: 'sIcon',
                            type: 'rect',
                            rect: ['50px', '333px', '50', '50', 'auto', 'auto'],
                            opacity: '1',
                            userClass: ""
                        }
                    ],
                    style: {
                        '${Stage}': {
                            isStage: true,
                            rect: ['null', 'null', '1200px', '608px', 'auto', 'auto'],
                            overflow: 'hidden',
                            fill: ["rgba(255,255,255,1)"]
                        }
                    }
                },
                timeline: {
                    duration: 0,
                    autoPlay: true,
                    data: [

                    ]
                }
            },
            "sIcon": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            type: 'group',
                            id: 'icon',
                            rect: ['0', '0px', '349px', '50', 'auto', 'auto'],
                            userClass: 'ltx-extra-icon',
                            c: [
                            {
                                type: 'text',
                                rect: ['66px', '-44px', '290px', '44px', 'auto', 'auto'],
                                id: 'extraInstructionText',
                                text: '<p style=\"margin: 0px;\">​What tasks does this involve?<br></p>',
                                userClass: 'text text-inst black',
                                font: ['Arial, Helvetica, sans-serif', [24, ''], 'rgba(0,0,0,1)', 'normal', 'none', '', 'break-word', 'normal']
                            },
                            {
                                type: 'group',
                                id: 'btn',
                                rect: ['0', '0', '50', '50', 'auto', 'auto'],
                                userClass: 'btn',
                                c: [
                                {
                                    type: 'image',
                                    id: 'img',
                                    rect: ['0', '0', '50px', '50px', 'auto', 'auto'],
                                    userClass: 'ignore-pointer',
                                    fill: ['rgba(0,0,0,0)', 'images/media-icon.svg', '0px', '0px']
                                }]
                            },
                            {
                                userClass: 'ltx-extra-content-holder',
                                rect: ['50px', '0px', '0px', '50px', 'auto', 'auto'],
                                id: 'extraContentHolder',
                                stroke: [0, 'rgb(0, 0, 0)', 'none'],
                                type: 'rect',
                                fill: ['rgba(192,192,192,1)']
                            }]
                        }
                    ],
                    style: {
                        '${symbolSelector}': {
                            rect: [null, null, '50px', '50px']
                        }
                    }
                },
                timeline: {
                    duration: 0,
                    autoPlay: true,
                    data: [

                    ]
                }
            },
            "sStandupPic": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            type: 'image',
                            id: 'pic_0',
                            rect: ['0px', '0px', '1200px', '608px', 'auto', 'auto'],
                            fill: ['rgba(0,0,0,0)', 'images/pic_0.jpg', '0px', '0px']
                        }
                    ],
                    style: {
                        '${symbolSelector}': {
                            rect: [null, null, '1200px', '608px']
                        }
                    }
                },
                timeline: {
                    duration: 0,
                    autoPlay: true,
                    data: [

                    ]
                }
            },
            "sIconNext": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: 'next-icon',
                            type: 'image',
                            rect: ['0px', '0px', '50px', '50px', 'auto', 'auto'],
                            fill: ['rgba(0,0,0,0)', 'images/next-icon.svg', '0px', '0px']
                        }
                    ],
                    style: {
                        '${symbolSelector}': {
                            rect: [null, null, '50px', '50px']
                        }
                    }
                },
                timeline: {
                    duration: 0,
                    autoPlay: true,
                    data: [

                    ]
                }
            },
            "sIconPrev": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: 'prev-icon',
                            type: 'image',
                            rect: ['0px', '0px', '50px', '50px', 'auto', 'auto'],
                            fill: ['rgba(0,0,0,0)', 'images/prev-icon.svg', '0px', '0px']
                        }
                    ],
                    style: {
                        '${symbolSelector}': {
                            rect: [null, null, '50px', '50px']
                        }
                    }
                },
                timeline: {
                    duration: 0,
                    autoPlay: true,
                    data: [

                    ]
                }
            },
            "sContent1": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            rect: ['0px', '0px', '1200px', '608px', 'auto', 'auto'],
                            id: 'pic_1',
                            userClass: 'debug',
                            type: 'image',
                            fill: ['rgba(0,0,0,0)', 'images/pic_1.jpg', '0px', '0px']
                        },
                        {
                            textStyle: ['', '', '', '', 'none'],
                            rect: ['45px', '30px', '310px', '196px', 'auto', 'auto'],
                            font: ['arimo, sans-serif', [18, 'px'], 'rgba(0,0,0,1)', '400', 'none', 'normal', 'break-word', 'normal'],
                            userClass: 'text bubble c_light-grey ignore-para shadow-2',
                            id: 'Text',
                            text: '<p style=\"margin: 0px;\">​•\tDesigning the website</p><p style=\"margin: 0px;\">•\tDeveloping the web content</p><p style=\"margin: 0px;\">•\tClient-side development</p><p style=\"margin: 0px;\">•\tServer-side development</p><p style=\"margin: 0px;\">•\tConfiguration activities</p>',
                            align: 'left',
                            type: 'text'
                        }
                    ],
                    style: {
                        '${symbolSelector}': {
                            isStage: 'true',
                            rect: [undefined, undefined, '1200px', '608px']
                        }
                    }
                },
                timeline: {
                    duration: 0,
                    autoPlay: true,
                    data: [

                    ]
                }
            },
            "sContent2": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            userClass: 'debug',
                            id: 'pic_1',
                            type: 'image',
                            rect: ['0px', '0px', '1200px', '608px', 'auto', 'auto'],
                            fill: ['rgba(0,0,0,0)', 'images/pic_2.jpg', '0px', '0px']
                        },
                        {
                            type: 'text',
                            rect: ['90px', '154px', '374px', '190px', 'auto', 'auto'],
                            align: 'left',
                            text: '<p style=\"margin: 0px;\">​Client-side development is also known as front-end development. This includes developing the web pages that the users see when they load a website. It may include web pages that contain forms, tables, pictures or other content. The design of the website, layouts and interactivity is mainly handled by the client-side development. We use HTML (Hypertext Markup Language), CSS (Cascading Style Sheets) and JavaScript for client-side development.</p>',
                            id: 'Text',
                            textStyle: ['', '', '', '', 'none'],
                            userClass: 'text bubble c_white shadow-2',
                            font: ['arimo, sans-serif', [18, 'px'], 'rgba(0,0,0,1)', '400', 'none', 'normal', 'break-word', 'normal']
                        }
                    ],
                    style: {
                        '${symbolSelector}': {
                            rect: [null, null, '1200px', '608px']
                        }
                    }
                },
                timeline: {
                    duration: 0,
                    autoPlay: true,
                    data: [

                    ]
                }
            },
            "sContent3": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            rect: ['0px', '0px', '1200px', '608px', 'auto', 'auto'],
                            id: 'pic_1',
                            userClass: 'debug',
                            type: 'image',
                            fill: ['rgba(0,0,0,0)', 'images/pic_3.jpg', '0px', '0px']
                        },
                        {
                            textStyle: ['', '', '', '', 'none'],
                            rect: ['294px', '165px', '612px', '388px', 'auto', 'auto'],
                            font: ['arimo, sans-serif', [18, 'px'], 'rgba(0,0,0,1)', '400', 'none', 'normal', 'break-word', 'normal'],
                            userClass: 'text bubble c_white solid shadow-1',
                            id: 'Text',
                            text: '<p style=\"margin: 0px;\">​Server-side development is also known as back-end development. This includes development and handling of the database that supports and contains the data of the website, applications that run the logic of the website, and the server that hosts the website. This database will include all data that is processed in the website. The code for server-side development can be written using many programming languages. Some examples are Java, Asp.NET and PHP.</p><p style=\"margin: 0px;\">In this topic we will mainly explore the client-side development using HTML. In this course we will also discuss about HTML5 (the latest version of HTML) and CSS.</p>',
                            align: 'left',
                            type: 'text'
                        }
                    ],
                    style: {
                        '${symbolSelector}': {
                            isStage: 'true',
                            rect: [undefined, undefined, '1200px', '608px']
                        }
                    }
                },
                timeline: {
                    duration: 0,
                    autoPlay: true,
                    data: [

                    ]
                }
            },
            "sIconClose": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: 'close-icon',
                            type: 'image',
                            rect: ['0px', '0px', '50px', '50px', 'auto', 'auto'],
                            fill: ['rgba(0,0,0,0)', 'images/close-icon.svg', '0px', '0px']
                        }
                    ],
                    style: {
                        '${symbolSelector}': {
                            rect: [null, null, '50px', '50px']
                        }
                    }
                },
                timeline: {
                    duration: 0,
                    autoPlay: true,
                    data: [

                    ]
                }
            },
            "sGlossary1": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            font: ['arimo, sans-serif', [18, 'px'], 'rgba(143,89,7,1.00)', '400', 'none', '', 'break-word', ''],
                            type: 'text',
                            id: 'Text',
                            text: '<p style=\"margin: 0px;\">HTML</p><p style=\"margin: 0px;\">‘Hypertext Markup Language’. This is the language that web servers use to tell your browser what content to display, and what colours, fonts, etc., to use.</p>',
                            userClass: 'text-h3 glossary',
                            rect: ['0px', '0px', '384px', '151px', 'auto', 'auto']
                        }
                    ],
                    style: {
                        '${symbolSelector}': {
                            rect: [null, null, '384px', '151px']
                        }
                    }
                },
                timeline: {
                    duration: 0,
                    autoPlay: true,
                    data: [

                    ]
                }
            }
        };

    AdobeEdge.registerCompositionDefn(compId, symbols, fonts, scripts, resources, opts);

    if (!window.edge_authoring_mode) AdobeEdge.getComposition(compId).load("6-6-3_edgeActions.js");
})("EDGE-5302340");
