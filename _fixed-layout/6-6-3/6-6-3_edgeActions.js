/***********************
* Adobe Edge Animate Composition Actions
*
* Edit this file with caution, being careful to preserve 
* function signatures and comments starting with 'Edge' to maintain the 
* ability to interact with these actions from within Adobe Edge Animate
*
***********************/
(function($, Edge, compId){
var Composition = Edge.Composition, Symbol = Edge.Symbol; // aliases for commonly used Edge classes

   //Edge symbol: 'stage'
   (function(symbolName) {
      
      
      Symbol.bindElementAction(compId, symbolName, "document", "compositionReady", function(sym, e) {
         // Enables CSS when you run the screen.
         $("#Stage").addClass("enable-css"); 
         var contentArray = new Array;
         
         //////////////
         // SETTINGS //
         //////////////////////////////////////////////////////////////////////////////////////////////////////////
         
         // Add the below class to ONE element in Content 1 to position the circular navigation buttons from the bottom right corner beneath it.
         // content-nav-target
         
         var crossFade = false;
         
         contentArray.push('audio1');
         contentArray.push('audio2');
         contentArray.push('audio2');
         
         //////////////////////////////////////////////////////////////////////////////////////////////////////////
         
         var contentHolder;
         var generatedContentStyle = "position: absolute; width: 100%; height: 100%; left: 0; top: 0;";
         
         var prevBtn;
         var nextBtn;
         var direction = 0;
         
         var animDuration = 500;
         
         var contentIndex = -1;
         
         init();
         
         function init(){
         	sym.$('sIcon').addClass("ltx-extra");
         	sym.$('sIcon').click(function(){
         		sym.$('sIcon').addClass('badge');//.prev(".text-inst-indent").addClass("badge");
         
         		// New implementation:
         		if (contentHolder == undefined)
         		{
         			direction = 'init';
         			contentIndex = 1;
         
         			$("#Stage").append("<div id='id-t13b-content-holder' style='"+generatedContentStyle+"'><div class='t13-content-bg'></div></div>");
         			contentHolder = $("#id-t13b-content-holder");
         			contentHolder.hide();
         
         			/* Initiate all content before each button is clicked
         			(means that all necessary loading of content and calculations
         			for extras is loaded before being revealed while the user reads
         			the standup). */
         			for (var i = 0; i < contentArray.length; i++)
         			{
         				var j = i+1;
         				createSmartChildSymbol(sym, 'sContent'+j, contentHolder).getSymbolElement().hide();
         			}
         
         			showContent(1);
         			initNavigation();
         		}
         		contentHolder.fadeIn(animDuration);
         
         		// Reset the custom Edge animation on the content (if there is one).
         		sym.content.play(0);
         	});
         }
         
         // Generates navigation over the content.
         function initNavigation(){
         
         	var closeBtn = sym.createChildSymbol('sIconClose', contentHolder).getSymbolElement();
         	closeBtn.css({"top":"20px", "left":"1130px", "position":"absolute", "z-index":1}).addClass("btn");
         
         	closeBtn.click(function(){
         
         		pauseAllVideos();
         
          		contentHolder.fadeOut(animDuration);
          		stopAllAudio(); 
          	});
         
          	// No need for navigation on a single content array.
          	if (contentArray.length == 1) return;
         
         	prevBtn = sym.createChildSymbol('sIconPrev', contentHolder).getSymbolElement();
         	nextBtn = sym.createChildSymbol('sIconNext', contentHolder).getSymbolElement();
         
         	prevBtn.css({"top":"279px", "left":"20px", "position":"absolute", "z-index":1}).addClass("btn");
         	nextBtn.css({"top":"279px", "left":"1130px", "position":"absolute", "z-index":1}).addClass("btn");
         
         	contentHolder.addContentNavigation(sym, contentArray.length, replaceContent);
         
         	prevBtn.hide();
         
         	nextBtn.click(function(){
         
         		replaceContent(contentIndex+1);
         	});
         
         	prevBtn.click(function(){
         
         		replaceContent(contentIndex-1);
         	});
         }
         
         function showContent(num){
         
         	pauseAllVideos();
         
         	sym.content = createSmartChildSymbol(sym, 'sContent'+num, contentHolder);
         
         	$(".content-nav div").removeClass("active");
         	$(".content-nav div:nth-child("+num+")").addClass("badge").addClass("active");
         
         	var e = sym.content.getSymbolElement();
         	// Different transition types.
         	if (!crossFade)
         	{
         		e.css({'position':'absolute', 'left':direction+"%"});
         		e.animate({'left':"0%"}, animDuration);
         	}
         	else
         	{
         		e.css({'position':'absolute', 'opacity':0});
         		e.animate({'opacity':"1"}, animDuration);
         	}
         
         	// Change the background of the holder (done in order to crossfade the background).
         	contentHolder.crossfadeBackground(e);
         
         	playSound(num);
         }
         
         function replaceContent(num){
         
         	if (contentIndex == num) return; // Don't re-reveal the same content.
         	if ($(".t13-content-bg").is(":animated")) return;
         
         	direction = (contentIndex > num) ? -100 : 100;
         
         	contentIndex = num;
         
         	var temp = sym.content;
         
         	if (!crossFade) temp.getSymbolElement().animate({"left":-(direction)+"%"}, animDuration);
         	else temp.getSymbolElement().animate({"opacity":"0"}, animDuration);
         
         	setTimeout(function(){ temp.deleteSymbol(); }, animDuration);
         	showContent(contentIndex);
         
         	if(contentIndex >= (contentArray.length)) nextBtn.fadeOut();
         	else nextBtn.fadeIn();
         	if(contentIndex <= 1) prevBtn.fadeOut();
         	else prevBtn.fadeIn();
         }
         
         function playSound(num){
         	stopAllAudio();
         	try{sym.$(contentArray[num-1])[0].play();} catch(err) { /*ignore if an error is thrown*/ }
         }
         
         function stopAllAudio(){
         	$.each(sym.$('audio'), function(index, value) {
         		if(!$(this)[0].paused){
         			$(this)[0].currentTime = 0;
         			$(this)[0].pause();
         		}
          	});
         }

      });
      //Edge binding end

   })("stage");
   //Edge symbol end:'stage'

   //=========================================================
   
   //Edge symbol: 'sIcon1'
   (function(symbolName) {   
   
   })("sIcon");
   //Edge symbol end:'sIcon'

   //=========================================================
   
   //Edge symbol: 'sStandupPic'
   (function(symbolName) {   
   
   })("sStandupPic");
   //Edge symbol end:'sStandupPic'

   //=========================================================
   
   //Edge symbol: 'sIconNext'
   (function(symbolName) {   
   
   })("sIconNext");
   //Edge symbol end:'sIconNext'

   //=========================================================
   
   //Edge symbol: 'sIconPrev'
   (function(symbolName) {   
   
   })("sIconPrev");
   //Edge symbol end:'sIconPrev'

   //=========================================================
   
   //Edge symbol: 'sContent1'
   (function(symbolName) {   
   
   })("sContent1");
   //Edge symbol end:'sContent1'

   //=========================================================
   
   //Edge symbol: 'sContent1_1'
   (function(symbolName) {   
   
      Symbol.bindSymbolAction(compId, symbolName, "creationComplete", function(sym, e) {
         sym.$("Text").glossaryText(
         	sym,
         	[
         		new Array("sGlossary1", "HTML")
         		//new Array("sGlossary2", "market") // Uncomment to add more terms for this element (be sure to add a ,).
         	],
         	""	// If you have multiple instances of the glossary term, and want to select a specific one, add a suffix to it and set this value to that suffix (it will automatically be removed from your text when the screen is viewed).
         );

      });
      //Edge binding end

      })("sContent2");
   //Edge symbol end:'sContent2'

   //=========================================================
   
   //Edge symbol: 'sContent1_2'
   (function(symbolName) {   
   
      })("sContent3");
   //Edge symbol end:'sContent3'

   //=========================================================
   
   //Edge symbol: 'sIconClose'
   (function(symbolName) {   
   
   })("sIconClose");
   //Edge symbol end:'sIconClose'

   //=========================================================

   //=========================================================

   //=========================================================

   //=========================================================

   //=========================================================

   //=========================================================

   //=========================================================

   //=========================================================

   //=========================================================

   //=========================================================
   
   //Edge symbol: 'sGlossary1'
   (function(symbolName) {   
   
      

      Symbol.bindSymbolAction(compId, symbolName, "creationComplete", function(sym, e) {
         /*
         sym.$("Text").glossaryText(
         	sym,
         	[
         		new Array("sGlossary1", "selling")
         		//new Array("sGlossary2", "market") // Uncomment to add more terms for this element (be sure to add a ,).
         	],
         	""	// If you have multiple instances of the glossary term, and want to select a specific one, add a suffix to it and set this value to that suffix (it will automatically be removed from your text when the screen is viewed).
         );
         */

      });
      //Edge binding end

   })("sGlossary1");
   //Edge symbol end:'sGlossary1'

})(window.jQuery || AdobeEdge.$, AdobeEdge, "EDGE-5302340");