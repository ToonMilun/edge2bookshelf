/*jslint */
/*global AdobeEdge: false, window: false, document: false, console:false, alert: false */
(function (compId) {

    "use strict";
    var im='images/',
        aud='media/',
        vid='media/',
        js='js/',
        fonts = {
            'arimo, sans-serif': '<script src=\"http://use.edgefonts.net/arimo:n4,i4,n7,i7:all.js\"></script>'        },
        opts = {
            'gAudioPreloadPreference': 'auto',
            'gVideoPreloadPreference': 'auto'
        },
        resources = [
        ],
        scripts = [
        ],
        symbols = {
            "stage": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: 'pic_0',
                            type: 'image',
                            rect: ['0px', '0px', '1200px', '608px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",im+"pic_0.jpg",'0px','0px']
                        },
                        {
                            id: 'T4Container',
                            type: 'rect',
                            rect: ['-460px', '0px', '380px', '608px', 'auto', 'auto'],
                            opacity: '0',
                            fill: ["rgba(208,218,227,0.95)"],
                            stroke: [0,"rgba(0,0,0,1)","none"],
                            userClass: "c_light-blue solid shadow-2",
                            c: [
                            {
                                id: 'Text',
                                type: 'text',
                                rect: ['51px', '110px', '280px', '510px', 'auto', 'auto'],
                                opacity: '1',
                                text: "<p style=\"margin: 0px;\">​Web basics</p><p style=\"margin: 0px;\">In this topic you will learn<br>about the following:</p><p style=\"margin: 0px;\">The basics of web browsing and related terminologies</p><p style=\"margin: 0px;\">Applying the concepts of basic HTML tags in web development</p><p style=\"margin: 0px;\">Describing the purpose of tables and forms</p><p style=\"margin: 0px;\">Implementing HTML<br>elements such as tables, forms and form elements</p>",
                                userClass: "text-h1 bullet ignore-first",
                                font: ['arimo, sans-serif', [35, "px"], "rgba(27,83,120,1.00)", "normal", "none", "", "break-word", "normal"],
                                textStyle: ["", "", "35px", "", ""]
                            }]
                        }
                    ],
                    style: {
                        '${Stage}': {
                            isStage: true,
                            rect: ['null', 'null', '1200px', '608px', 'auto', 'auto'],
                            overflow: 'hidden',
                            fill: ["rgba(255,255,255,1)"]
                        }
                    }
                },
                timeline: {
                    duration: 1000,
                    autoPlay: true,
                    data: [
                        [
                            "eid16",
                            "left",
                            0,
                            1000,
                            "easeOutBack",
                            "${T4Container}",
                            '-460px',
                            '0px'
                        ],
                        [
                            "eid15",
                            "opacity",
                            0,
                            1000,
                            "easeOutBack",
                            "${T4Container}",
                            '0',
                            '1'
                        ]
                    ]
                }
            }
        };

    AdobeEdge.registerCompositionDefn(compId, symbols, fonts, scripts, resources, opts);

    if (!window.edge_authoring_mode) AdobeEdge.getComposition(compId).load("6-6-1_edgeActions.js");
})("EDGE-5302340");
