/*jslint */
/*global AdobeEdge: false, window: false, document: false, console:false, alert: false */
(function (compId) {

    "use strict";
    var im='images/',
        aud='media/',
        vid='media/',
        js='js/',
        fonts = {
            'arimo, sans-serif': '<script src=\"http://use.edgefonts.net/arimo:n4,i4,n7,i7:all.js\"></script>'        },
        opts = {
            'gAudioPreloadPreference': 'auto',
            'gVideoPreloadPreference': 'auto'
        },
        resources = [
        ],
        scripts = [
        ],
        symbols = {
            "stage": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: 'pic_0',
                            type: 'image',
                            rect: ['0', '0', '1200px', '608px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",im+"pic_0.jpg",'0px','0px']
                        },
                        {
                            id: 'Text',
                            type: 'text',
                            rect: ['50px', '35px', '1100px', '104px', 'auto', 'auto'],
                            text: "<p style=\"margin: 0px;\">​​Opening thoughts</p><p style=\"margin: 0px;\">​How do today’s customers use the internet to find products and services?</p>",
                            align: "left",
                            userClass: "text-h1 text-inst",
                            font: ['arimo, sans-serif', [35, "px"], "rgba(27,83,120,1)", "400", "none", "normal", "break-word", "normal"],
                            textStyle: ["", "", "", "", "none"]
                        },
                        {
                            id: 'video',
                            type: 'video',
                            tag: 'video',
                            rect: ['50px', '167px', '640px', '363px', 'auto', 'auto'],
                            controls: 'controls',
                            source: [vid+"video.mp4"],
                            preload: 'auto',
                            poster: 'images/poster.jpg'
                        },
                        {
                            id: 'sTranscriptControl',
                            symbolName: 'sTranscriptControl',
                            type: 'rect',
                            rect: ['720px', '490px', 'undefined', 'undefined', 'auto', 'auto']
                        }
                    ],
                    style: {
                        '${Stage}': {
                            isStage: true,
                            rect: ['null', 'null', '1200px', '608px', 'auto', 'auto'],
                            overflow: 'hidden',
                            fill: ["rgba(255,255,255,1)"]
                        }
                    }
                },
                timeline: {
                    duration: 0,
                    autoPlay: true,
                    data: [

                    ]
                }
            },
            "sTranscriptControl": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            rect: ['0', '0', '50', '50', 'auto', 'auto'],
                            id: 'icon',
                            type: 'group',
                            userClass: 'btn',
                            c: [
                            {
                                type: 'image',
                                id: 'transcript-icon',
                                rect: ['0px', '0px', '50px', '50px', 'auto', 'auto'],
                                fill: ['rgba(0,0,0,0)', 'images/transcript-icon.svg', '0px', '0px']
                            }]
                        }
                    ],
                    style: {
                        '${symbolSelector}': {
                            rect: [null, null, '50px', '50px']
                        }
                    }
                },
                timeline: {
                    duration: 0,
                    autoPlay: true,
                    data: [

                    ]
                }
            },
            "sTranscript": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            rect: ['0px', '0px', '440px', '608px', 'auto', 'auto'],
                            userClass: 'c_white solid',
                            id: 'Panel',
                            stroke: [0, 'rgb(0, 0, 0)', 'none'],
                            type: 'rect',
                            fill: ['rgba(0,0,0,0.75)']
                        },
                        {
                            type: 'text',
                            align: 'left',
                            text: '<p style=\"margin: 0px;\">​In the current world, the chances are if you are a business owner, you own a website to promote it as well. If not, you may be losing many opportunities that could help your business grow. </p><p style=\"margin: 0px;\">Today’s customers are mainly relying on the internet to find products and services they require.</p><p style=\"margin: 0px;\">Most customers prefer to look up products and services online by visiting the business’s website, even if they don’t plan to buy them online. Other than for looking up products and services, business websites help maintain product knowledge, maintain communication with your customers and generate more business opportunities.&nbsp;</p><p style=\"margin: 0px;\">So, in such a world, it is vital that your business is promoted via a website. A well-developed website helps the business appear more reliable, attractive and assists to tap the global market.</p>',
                            userClass: 'text scroll',
                            overflow: 'visible',
                            textStyle: ['', '', '', '', 'none'],
                            font: ['arimo, sans-serif', [18, 'px'], 'rgba(0,0,0,1.00)', '400', 'none', 'normal', 'break-word', 'normal'],
                            rect: ['40px', '85px', '360px', '484px', 'auto', 'auto'],
                            id: 'Text'
                        },
                        {
                            textStyle: ['', '', '25px', '', 'none'],
                            rect: ['40px', '35px', '310px', '25px', 'auto', 'auto'],
                            font: ['arimo, sans-serif', [25, 'px'], 'rgba(27,83,120,1.00)', '400', 'none', 'normal', 'break-word', 'normal'],
                            userClass: 'text-h2',
                            id: 'Heading',
                            text: '<p style=\"margin: 0px;\">​Transcript</p>',
                            align: 'left',
                            type: 'text'
                        },
                        {
                            rect: ['370px', '20px', '50', '50', 'auto', 'auto'],
                            id: 'btnClose',
                            type: 'group',
                            userClass: 'btn',
                            c: [
                            {
                                id: 'close-icon',
                                type: 'image',
                                rect: ['0px', '0px', '50px', '50px', 'auto', 'auto'],
                                fill: ['rgba(0,0,0,0)', 'images/close-icon.svg', '0px', '0px']
                            }]
                        }
                    ],
                    style: {
                        '${symbolSelector}': {
                            isStage: 'true',
                            rect: [undefined, undefined, '440px', '608px']
                        }
                    }
                },
                timeline: {
                    duration: 0,
                    autoPlay: true,
                    data: [

                    ]
                }
            }
        };

    AdobeEdge.registerCompositionDefn(compId, symbols, fonts, scripts, resources, opts);

    if (!window.edge_authoring_mode) AdobeEdge.getComposition(compId).load("6-6-2_edgeActions.js");
})("EDGE-5302340");
