/***********************
* Adobe Edge Animate Composition Actions
*
* Edit this file with caution, being careful to preserve 
* function signatures and comments starting with 'Edge' to maintain the 
* ability to interact with these actions from within Adobe Edge Animate
*
***********************/
(function($, Edge, compId){
var Composition = Edge.Composition, Symbol = Edge.Symbol; // aliases for commonly used Edge classes

   //Edge symbol: 'stage'
   (function(symbolName) {
      
      
      Symbol.bindElementAction(compId, symbolName, "document", "compositionReady", function(sym, e) {
         // Enables CSS when you run the screen.
         $("#Stage").addClass("enable-css");

      });
      //Edge binding end

   })("stage");
   //Edge symbol end:'stage'

   //=========================================================

   //=========================================================

   //=========================================================
   
   //Edge symbol: 'sTranscriptControl'
   (function(symbolName) {   
   
      

      

      Symbol.bindSymbolAction(compId, symbolName, "creationComplete", function(sym, e) {
         // insert code to be run when the symbol is created here
         initTranscript(sym, this.getSymbolElement(), "sTranscript");

      });
      //Edge binding end

   })("sTranscriptControl");
   //Edge symbol end:'sTranscriptControl'

   //=========================================================
   
   //Edge symbol: 'sTranscript'
   (function(symbolName) {   
   
      

   })("sTranscript");
   //Edge symbol end:'sTranscript'

})(window.jQuery || AdobeEdge.$, AdobeEdge, "EDGE-5302340");