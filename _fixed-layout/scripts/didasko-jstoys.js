// Last edited: May 2018

/*********************/
/*					 */
/* 	JS TOYS SCRIPTS  */
/*					 */
/***************************************************************************************************/
/* Do NOT make changed to this script unless it is in the root of the "screens" directory!		   */
/* This script is batch copied/updated into all screen directories, and may be replaced otherwise. */
/***************************************************************************************************/

console.log("-= JS Toys script loaded successfully =-");

/*********************/
/* SEGMENTED PYRAMID */
/*********************/
$.fn.JSTPyramid = function(segmentArr, squashPercent) {
	
	initJSTScreen($(this), "jst-pyramid");
	
	var $me = $(this);
	var height = $me.outerHeight();
	
	var segmentHeight = height / segmentArr.length;
	var segmentWidthInc = (1 / segmentArr.length) * 105;
	
	var oldWidth = $me.width();
	var newWidth = $me.height()*1.10;
	$me.css({
		"width": newWidth,
		"left": "+= " + ((oldWidth-newWidth)/2)
	});
	
	// Generate the elements inside the holder that will be used.
	segmentArr.forEach(function(e,i){
		
		var w = (segmentWidthInc * (i+1));
		var t = 'style="-webkit-transform: scaleX(' + squashPercent + '); transform: scaleX(' + squashPercent + ');"';
		
		$me.append('\
		<div class="jst-pyramid-segment-wrapper btn-xl" style=" \
			height:' + segmentHeight + 'px; \
			width:' + w + '%; \
			margin-left: ' + ((100-w)/2) + '%; \
			z-index: ' + (100-i) + '"> \
			<div ' + t + '><div style="background-color: ' + e.bgcolor + ';"></div></div> \
			<div ' + t + '><div style="background-color: ' + e.bgcolor + ';"></div></div> \
			<div class="text-h1 ignore-color center string" style="color: rgba(255,255,255,0.9); font-weight: bold;"><p style="margin-top: ' + ((i == 0) ? 70 : 10) + '%;">' + e.string + '</p></div> \
			<div hidden style="position: absolute; left: 50%; margin-left: -10px; top: 30px; width: 20px; height: 20px; background-image: url(images/badge.svg);"></div> \
		</div>');
		
		$segmentWrapper = $me.find(".jst-pyramid-segment-wrapper").last();
		
		$segmentWrapper.click(function(){
			
			$(this).find("[style*=badge]").show();
			e.click();
		});
	});
};

/******************/
/* HELPER SCRIPTS */
/******************/
function initJSTScreen($holder, jstName) {
	
	// Add the didasko-jst-styles.css to the header.
	$("head").append('<link rel="stylesheet" type="text/css" href="css/didasko-jst-styles.css">');
	
	// All holders need to have the .jstoy class.
	$holder.addClass("jstoy");
	$holder.addClass(jstName);
}










