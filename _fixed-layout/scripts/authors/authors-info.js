///////////////////////////////////////////////
// Pseudo-database of all author information //
///////////////////////////////////////////////

// Yes, I know it's bad to use a .js file for this. It's done this way to avoid Cross-Domain access issues.
// If the data gets moved to an XML, just modify the access function below. - Milton Plotkin, 2018

var IMG_DIRECTORY = "assets/slides/scripts/authors/";

var authors = {};

/* The filename for the image is automatically determined based on the name. */
authors["DUMMY"] = {
	role: "DUMMY",
	body: "Insert description here.",
	hide: true // Set to true to remove any references of this author from ALL content, past and future. Unlikely to be ever used, but left in just in case.
};
authors["Stephanie Jaehrling"] = {
	role: "Editor",
	body: "Stephanie Jaehrling is a freelance editor and proofreader with a diverse range of clients. She has been an editor at Didasko since 2014, editing and proofreading their online education content and support materials. She was an in-house editor at the University of Melbourne for over ten years, where she edited and managed the production of a wide range of publications, including exhibition materials, books, magazines, annual reports, newsletters, manuals, brochures, and many other documents."
};
authors["Chris Swan"] = {
	role: "Writer",
	body: "Christopher Swan is an early career academic, coordinating and lecturing in management studies at Didasko in partnership with La Trobe University. He holds consultancy and industry publication experience in this space and has advised a range of organisations on matters of management/employee behaviour, business strategy and new venture creation. His research interests and academic publications include the motivation and behaviour of non-traditional entrepreneurial types and the process of ecotourism business start-up, ownership and operation."
};
authors["Kathy McInnes"] = {
	role: "Writer",
	body: "An accomplished technical writer, Kathy McInnes has 16 years’ industry experience writing and developing quality training and educational resources. She synthesises, reads and accurately interprets third-party requirements, and writes carefully researched content obtained from key stakeholders, texts, websites and external agencies. As a research writer, Kathy’s skillset is unique and she has successfully adapted her writing across many industries."
};
authors["Naomi Holding"] = {
	role: "Author",
	body: "Naomi Holding is the General Manager of Education at Didasko where she has also held the positions of Project Manager and Subject Matter Expert/Research Writer.</p><p>Naomi has extensive experience in lecturing, teaching, writing and curriculum design and development across universities, TAFE and private registered training organisations.  Her teaching experience and interest includes Human Resource Management, Organisational Behaviour, Leadership, Accommodation Management and Managing Diversity.</p><p>Naomi holds an MBA, B. Ed. & Training, Dip in Teaching, Diploma in Hospitality and Cert. IV in Training and Assessment and is a member of AHRI."
};
authors["Dennis Alphonsus"] = {
	role: "???",
	body: "???",
	hide: true // Remove when description is added.
};
authors["Tom Davies"] = {
	role: "???",
	body: "???",
	hide: true // Remove when description is added.
};
authors["Judy James"] = {
	role: "???",
	body: "???",
	hide: true // Remove when description is added.
};
authors["Yan Zhang"] = {
	role: "Author on Project management",
	body: "Yan has over ten years’ experience in university environments. She had worked as a lecturer at La Trobe University for 5 years, a post-doctoral researcher for the Federal Department of Education, Science & Training, and a research assistant in the Parliament of Victoria. Dr. Zhang’s research projects are regularly published in a variety of high-level international academic publications, including an international marketing project which won two national awards.</p><p>She has also worked in the retail, wholesale, tourism and hospitality industries for more than ten years. Yan has successfully set up and managed her own retail shops, online stores, and is a partner of a 120-room Airbnb business."
};
authors["David Strachan"] = {
	role: "", // TBA
	body: "",  // TBA
	hide: true // Remove when description is added.
};
authors["Madelyn Kinch"] = {
	role: "", // TBA
	body: "",  // TBA
	hide: true
};
authors["Dr. Mai Pham"] = {
	role: "", // TBA
	body: "",  // TBA
	hide: true // Remove when description is added.
};
authors["Didasko Digital Team"] = {
	role: "Production",
	body: "With more than 20 years of experience creating interactive multimedia for the education sector, this award-winning team have developed an adaptable learning model which draws on our philosophy that learning must be active, engaging and relevant. The team have extensive experience designing a range of digital learning interactions across simulated environments, animation, video and interactive pieces. The team, comprising writers, instructional designers and multimedia developers, pride themselves on creating digital learning interactions that challenge learners to apply the knowledge they’ve learned and to think both critically and creatively."
};

/* In future, have the role for each author change based on the last modified date of the dolCore.js calling getAuthorObject(). */

// If there is ever a double up on names, add a number suffix. The script will automatically remove it when it generates the HTML.
/*authors["Stephanie Jaehrling1"] = {
	body: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tempus dignissim neque, a iaculis magna commodo eu. Nam commodo metus in tortor gravida, ac sodales libero mollis. Etiam id sapien sed sem placerat mollis et eget nisl. Maecenas in augue neque. Ut dapibus magna non libero vulputate efficitur. Vestibulum eget urna augue. Donec convallis bibendum ligula. Donec ultricies arcu iaculis consectetur tristique. Morbi id elementum metus, sed gravida orci. Duis pulvinar imperdiet aliquet. Vivamus at mi lectus."
};*/

function getAuthorObject(xmlObject)
{	
	var authorName = xmlObject.children("name").text();
	var author = authors[authorName];
	
	if (author === undefined)
	{
		console.log("Error: " + authorName + " author could not be found.");
		return;
	}
	
	// Authors information has been manually hidden.
	if (author.hide == true)
	{
		return null;
	}
	
	// Allow for overriding the author's role with the local XML.
	var authorRole = xmlObject.children("role").text();
	if (authorRole == "") authorRole = author.role;
	
	var spl = authorName.split(" ");
	var src = IMG_DIRECTORY + "auth_";
	for (var i = 0; i < spl.length; i++)
	{
		// Skip name titles (like Dr.) for filenames.
		if (spl[i] == "Dr.") continue;
		
		src += spl[i];
		//if (i < spl.length-1) src += "-";
	}
	src += ".png";
	
	var output = {
		
		heading: (authorRole == "" ? "" : (authorRole + ": ")) + authorName.replace(/\d+/g, ''), // Remove any number suffices.
		body: authors[authorName].body,
		imgSrc: src
	}
	
	return output;
}