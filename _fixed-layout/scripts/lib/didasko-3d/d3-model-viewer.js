/**************************/
/* 3D Model viewer script */
/**************************/
/* Author: Milton Plotkin
/* Date: January 17th 2018
/* Function: Script to be loaded externally by Adobe Edge that will create a ThreeJS canvas on the screen.
/* Will load a specific .obj model and its .mtl, and will allow basic mouse/touch orbitControls to rotate/zoom it around.
/* Will also allow for hotspots to be clicked to reveal information on the object. */

function D3ModelViewer($container, modelName, options) {
	
	/***********/
	/* PRELOAD */
	/***********/
	
	var scriptdir = $('script[src*=d3-model-viewer]').attr('src').replace('d3-model-viewer.js', ''); // the js folder path
	
	// Load in dependencies (important to load Three.js first).
	$.when(
		$.getScript(scriptdir + "../didasko-scripts.js" ),
		$.getScript(scriptdir + "lib/d3-controller.js" )
	).done(function(){

		console.log("External scripts loaded.");
		
		options = initOptions(options, defaultOptions);
		
		d3c = new D3Controller(scriptdir, $container, init);
	});
	
	/***********/
	/* Options */
	/***********/
	
	var defaultOptions = {
		
		debug: 		false,
		enableZoom: false,
		position: 	[0,0,0],
		rotation: 	[0,0,0], // Won't do anything for now.
		scale: 		1,
		onClick:	function(hotspotName, clickedObject){}, // What happens whenever any hotspot is clicked (passes in the tag and the hotspot THREE object).
		hotspots: 	[/*tag: zoomTransform*/] // Will treat all objects which have their FIRST TAG match the key of an element in this array as a hotspot (meaning multiple objects CAN activate the same hotspot reveal).
	}
	
	
	/*************/
	/* VARIABLES */
	/*************/
	
	var d3c;
	
	var hotspots = {}; // Object that will hold many hotspot arrays as properties.
	var curHotspot = null;
	
	var CAMERA_ZOOMED = false;
	var cameraLastTransform = {};
	var cameraGoalTransform = {};
	
	/***********/
	/* METHODS */
	/***********/
	
	function init()
	{
		// Init an array of hotspot arrays (since there can be multiple THREE objects per hotspot.
		$.each(options.hotspots, function(key, val){
			
			if (hotspots[key] === undefined)
			{
				hotspots[key] = {
					objects: new Array(),
					zoomTransform: val
				};
			}
			else alert("Error: same hotspot (" + key + ") added twice.");
		});
		
		loadModel();
		
		d3c.InitOrbitControls(options.enableZoom);
		
		if (options.debug)
		{
			var o = d3c.GetOrbitControls();
			o.noPan = false;
			o.minPolarAngle = -360;
			o.maxPolarAngle = 360;
			o.minDistance = 0;
			
			// Show the cameras position and rotation (to assist with setting the "zoom in on mesh" values).
			$("body").append("<div class='d3-debug-camera-values' style='position: absolute; top: 620px; left: 20px;'><button>Copy</button><textarea>ERROR!</textarea></div>");
			var $debugCameraValues = $("body .d3-debug-camera-values:last-child textarea");
			var $btn = $("body .d3-debug-camera-values:last-child button").click(function(){
				
				$debugCameraValues.select();
				document.execCommand('copy');
			});
			
			d3c.SetOnStep(function(){
				
				var camera = d3c.GetCamera();
				
				var s = "{position: {x:" + camera.position.x.toFixed(2) + ", y:" + camera.position.y.toFixed(2) + ", z:"  + camera.position.z.toFixed(2) + "}, rotation: {x:" + camera.rotation.x.toFixed(2) + ", y:" + camera.rotation.y.toFixed(2) + ", z:" + camera.rotation.z.toFixed(2) + "}}";
				
				if ($debugCameraValues.text() != s)	$debugCameraValues.text(s);
			});
		}
	}
	
	function loadModel()
	{
		d3c.LoadModel("media\\"+modelName, modelName, function (object) {
			
			object.position.set(options.position[0], options.position[1], options.position[2]);
			object.scale.set(options.scale, options.scale, options.scale);
			
			//console.log(options);
			
			// Go through each model.
			object.traverse( function(child) {
				if (child instanceof THREE.Mesh) {
					
					var tags = d3c.GetNameTags(child.name);
					
					if (tags.length >= 1)
					{
						// Check if this child should be made into a hotspot.
						if (hotspots[tags[0]] !== undefined)
						{
							hotspots[tags[0]].objects.push(child);
						}
						
						// Set certain materials on this child to reflect.
						if (child.material.materials != undefined) {
							for (var i = 0; i < child.material.materials.length; i++) {
								
								SetMaterialToReflective(child.material.materials[i].name, child.material.materials[i], child);
							}
						}
						else
						{
							SetMaterialToReflective(child.material.name, child.material, child);
						}
					}
				}
			});
			
			d3c.AddObjectToScene(object);
			
			//////////////////////
			// Finished loading //
			//////////////////////
			
			initClick();
		});
	}
	
	function SetMaterialToReflective(materialType, material, child)
	{
		switch (materialType)
		{
			case "glass":
				child.renderOrder = 1000;
				d3c.SetMaterialToReflective(material, 0.4);
				break;
			
			case "gold":
				d3c.SetMaterialToReflective(material, 0.8);
				break;
			
			case "silver":
				d3c.SetMaterialToReflective(material, 0.1);
				break;
			
			case "super_silver":
				d3c.SetMaterialToReflective(material, 0.2);
				break;
			
			case "dark_silver":
				d3c.SetMaterialToReflective(material, 0.2);
				break;
		}
	}
	
	function initClick()
	{
		d3c.SetOnMouseClick(function(){
			
			if (CAMERA_ZOOMED) return;
			
			var distance = Infinity;
			var hotspot = null;		// Hotspot js object.
			var hotspotName = null; // Hotspot js object name;
			var object = null; 		// The THREE object clicked on.
			
			// Check through all hotspots that were raycast, then activate the nearests one.
			$.each(hotspots, function(key, val){
				
				var nearest = d3c.GetRaycastNearestHit(val.objects);
				
				if (nearest == null) return true; // Continue.
				
				if (hotspot == null || nearest.distance < distance)
				{
					distance = nearest.distance;
					hotspot = val;
					hotspotName = key;
					object = nearest.object;
				}
			});
			
			if (hotspot != null)
			{
				if (options.onClick != null) options.onClick(hotspotName, object);
				
				//console.log(hotspot);
				
				// Zoom in on object.
				zoomInOnObject(object, hotspot.zoomTransform);
			}
		});
	}
	
	function zoomInOnObject(object, zoomTransform) {

		if (CAMERA_ZOOMED) return;
	
		// Stop the user from being able to pan.
		var orbit = d3c.GetOrbitControls();
		orbit.enabled = false;
		orbit.state = -1;
		
		var camera = d3c.GetCamera();

		// Store the location the camera was in before the user clicked (only if the camera was not zoomed when they did so).
		cameraLastTransform.position = new THREE.Vector3(camera.position.x, camera.position.y, camera.position.z);
		cameraLastTransform.rotation = new THREE.Vector3(camera.rotation.x, camera.rotation.y, camera.rotation.z);
		CAMERA_ZOOMED = true;
		
		camera.position.x = zoomTransform.position.x;
		camera.position.y = zoomTransform.position.y;
		camera.position.z = zoomTransform.position.z;
		camera.rotation.x = zoomTransform.rotation.x;
		camera.rotation.y = zoomTransform.rotation.y;
		camera.rotation.z = zoomTransform.rotation.z;

		// Store the final location we want the camera to be in.
		cameraGoalTransform = {
			position : new THREE.Vector3(camera.position.x, camera.position.y, camera.position.z),
			rotation : new THREE.Vector3(camera.rotation.x, camera.rotation.y, camera.rotation.z)
		};

		// Then, put the camera back where it was.
		camera.position.x = cameraLastTransform.position.x;
		camera.position.y = cameraLastTransform.position.y;
		camera.position.z = cameraLastTransform.position.z;
		camera.rotation.x = cameraLastTransform.rotation.x;
		camera.rotation.y = cameraLastTransform.rotation.y;
		camera.rotation.z = cameraLastTransform.rotation.z;

		d3c.AnimateCamera(cameraGoalTransform, function(){
			
			
		}); 
		
		// Highlight the object (line thickness based on camera distance).
		//var camDist = Math.sqrt(helper.position.clone().sub(cameraGoalTransform.position).length())/1000;
		//highlightObject(obj, camDist);

		// Record the active object (used by Edge).
		//activeObject.obj = obj;
		
		// Call the show content function passed to this script.
		//onShowContent(num);
		//fadeInObject3D(obj);
		
		// Makes bounding box visible.
		//scene.add(helper);
	}
	
	// Temporary implementation.
	this.AnimateHotspot = function(hotspotName, position, onCompleteAction)
	{
		var o = hotspots[hotspotName].objects[0];
		
		TweenLite.to(o.position, 1, {
			x: position.x,
			y: position.y,
			z: position.z,
			onComplete: onCompleteAction
		});
	}
	
	// Manually/custom activated zoom in (called by Edge).
	this.ZoomIn = function(hotspotName)
	{
		zoomInOnObject(hotspots[hotspotName].objects[0], hotspots[hotspotName].zoomTransform);
	}
	
	this.ZoomOut = function()
	{
		if (!CAMERA_ZOOMED) return;
		
		var orbit = d3c.GetOrbitControls();
		
		d3c.AnimateCamera(cameraLastTransform, function(){
			
			CAMERA_ZOOMED = false;
			
			// Order is important. Prevents a flickering/incorrect position bug with the orbit controls.
			orbit.reset();
			
			var camera = d3c.GetCamera();
			
			camera.position.x = cameraLastTransform.position.x;
			camera.position.y = cameraLastTransform.position.y;
			camera.position.z = cameraLastTransform.position.z;
			camera.rotation.x = cameraLastTransform.rotation.x;
			camera.rotation.y = cameraLastTransform.rotation.y;
			camera.rotation.z = cameraLastTransform.rotation.z;
			
			orbit.update();
			orbit.enabled = true;
			orbit.state = 0;
		});
	}
}


















