/**************************/
/* 3D Model viewer script */
/**************************/
/* Author: Milton Plotkin
/* Date: February 16th 2016
/* Function: Script to be loaded externally by Adobe Edge that will create a ThreeJS canvas on the screen.
/* Will load a specific .obj model and its .mtl, and will allow basic mouse/touch orbitControls to rotate/zoom it around.
/* Will also allow for hotspots to be clicked to reveal information on the object. */

$.extend(jQuery.easing, {
	easeOutExpo : function (c, a, b, d, g) {
		return a == g ? b + d : d * (-Math.pow(2, -10 * a / g) + 1) + b
	}
});

function Hotspot(texture, position, scene) {
	
	var sprite = new THREE.Sprite( new THREE.SpriteMaterial( { map: texture, color: 0xffffff, depthTest: false } ) );
	sprite.scale.set(20, 20, 20);
	sprite.renderOrder = 100;
	scene.add( sprite );
	
	this.getObject = function(){
		return sprite;
	}
}

// Calls "onComplete" when all items have been viewed.
function HomeViewer(containerElement, adaptParent, modelName, onScreenComplete) {

	/*************/
	/* VARIABLES */
	/*************/

	var ddv;

	this.canClickToWalk = true; // If "true", then if the user clicks it will make them travel. Is set to true whenever the mouse is over a flat, horizontal floor.
	// Note: check to see if this implementation will still walk on iPads.

	this.fpControlsAction = function () {};
	this.fpControlsActive = false;

	this.cursor;
	
	this.CAMERA_HEIGHT;
	
	var hotspots = new Array();
	var activeHotspot = null; // The current hotspot that we have our mouse over (if any).
	
	var HOTSPOT_SIZE = 20;
	var reflectionCube;
	
	var roomCur = null;
	var lockInput = false;
	
	var outlineObject = null; // Creates an outline around a clicked on object.
	
	var o;
	
	var isMobile = false;
	
	/***************/
	/* SCENE SETUP */
	/***************/

	init();

	// https://discourse.threejs.org/t/outlining-a-partially-transparent-3d-model/284
	function createOutlineMaterial(r, g, b, a, thickness)
	{
		var outline_shader = {
			vertex_shader: [
				"uniform float offset;",
					"void main() {",
						"vec4 pos = modelViewMatrix * vec4( position + normal * "+thickness+", 1.0 );",
						"gl_Position = projectionMatrix * pos;",
					"}"
			].join("\n"),
			fragment_shader: [
				"void main(){",
					"gl_FragColor = vec4( "+r+", "+g+", "+b+", "+a+" );",
				"}"
			].join("\n")
		};

		var outline_material = new THREE.ShaderMaterial({
			vertexShader: outline_shader.vertex_shader,
			fragmentShader: outline_shader.fragment_shader,
			morphNormals: true
		});
		outline_material.side = THREE.DoubleSide;
		outline_material.transparent = true;
		//outline_material.depthTest = false;
		//outline_material.depthWrite = false;
		
		return outline_material;
	}
	
	this.setScreenIsMobile = function(val) {
		
		isMobile = val;
		hideContent(adaptParent.$(".narrative-3d-reveal")); // Hide content on screen transition to be on the safe side.
		adaptParent.animateHideContent();
	}
	
	function hideContent(e) {
		
		e.removeClass("active");
		adaptParent.$(".id-reveal-line").remove();
		setTimeout(function(){
			e.remove();
		}, 750);
		
		removeOutline(outlineObject);
	
		// Animate the full-width reveal away.
		if (isMobile) adaptParent.animateHideContent();
	}
	
	function showContent(num) {
		
		console.log("Showing content: " + num);
		
		// Simpler content reveal.
		if (isMobile)
		{
			//adaptParent.$(".narrative-mobile-holder").show();
			//adaptParent.$(".narrative-mobile-holder .narrative-content-item").hide();
			//adaptParent.$(".narrative-mobile-holder .narrative-content-item:nth-child("+num+")").show();
			
			adaptParent.animateRevealContent(num-1);
			
			ddv.tempCameraPos = ddv.camera.position.clone();
			
			return;
		}
		
		var title = adaptParent.$(".narrative-mobile-holder .narrative-content-item:nth-child("+num+") .narrative-content-title-inner").html();
		var body = adaptParent.$(".narrative-mobile-holder .narrative-content-item:nth-child("+num+") .narrative-content-body-inner").html();
		var contentClass = 'id-reveal-'+num;
		
		// Remove any previous content that was displayed.
		//adaptParent.$(".narrative-3d-reveal").slideUp(function(){this.remove();});
		var toRemove = adaptParent.$(".narrative-3d-reveal");
		
		// Don't re-hide/show the same content in succession.
		if (toRemove.hasClass(contentClass)) return;
		
		var revealDelay = 0;
		if (toRemove.length != 0)
		{
			revealDelay = 500;
			hideContent(toRemove);
		}
		
		// Draw a line from the point we clicked on to the content.
		adaptParent.$(".main-wrapper").append('<div class="id-reveal-line narrative-3d-desktop-only"></div>');
		var p = adaptParent.$(".main-wrapper");
		var line = adaptParent.$(".id-reveal-line");
		line.css({"left":p.width()*(ddv.mouse.x+1)/2,
				  "top":p.height()*(-ddv.mouse.y+1)/2});
		
		// Show the content in the most empty available space (assumed based on cursor location).
		var contentAlignment = (ddv.mouse.x >= 0) ? "right" : "left";
		
		adaptParent.$(".main-wrapper").append(' \
		<div class="narrative-3d-reveal narrative-3d-desktop-only narrative-content '+contentClass+'" style="overflow: hidden; position: absolute; width: 33% !important; min-width: 400px; '+contentAlignment+': 0; top: 30px;"> \
			<div class="narrative-content-inner"> \
				<div class="narrative-content-item"> \
					<div class="narrative-content-title"> \
						<h3 class="narrative-content-title-inner accessible-text-block h5" style="text-align: center" role="heading" aria-level="5" tabindex="0"> \
							'+title+' \
						</h3> \
					</div> \
					<div class="narrative-content-body"> \
						<div class="narrative-content-body-inner"> \
							'+body+' \
						</div> \
					</div> \
				</div> \
			</div> \
		</div>');
		
		var content = adaptParent.$("."+contentClass);
		
		var v = {
			x: ((contentAlignment == "left") ? (content.offset().left+content.width()-2) : content.offset().left+2) - line.offset().left,
			y: content.offset().top - line.offset().top
		}
		
		var h = Math.sqrt(v.x*v.x + v.y*v.y);
		
		var a = Math.atan2(v.y, v.x) * 180/Math.PI;
		line.css('transform', 'rotate('+a+'deg)');
		
		//console.log(ddv.mouse);
		
		setTimeout(function(){
			content.offset();
			content.addClass("active");
			line.animate({"width":h}, 500);
			ddv.tempCameraPos = ddv.camera.position.clone();
		}, revealDelay);
	}
	
	function init() {

		ddv = new DDViewer();
		ddv.init3D(containerElement, true);
		
		// Reflective materials [Gold and silver].
		var path = "course\\en\\images\\general\\3d-reflections\\tex_ref";
		var format = '.png';
		var urls = [
				path + '2' + format, path + '1' + format,
				path + '3' + format, path + '4' + format,
				path + '5' + format, path + '6' + format
			];

		reflectionCube = new THREE.CubeTextureLoader().load( urls );
		
		ddv.loadModel("course\\en\\other\\"+modelName, modelName, function (object) {
			
			object.scale.set(1.25, 1.25, 1.25);
			object.position.set(0, 14, 0);
			
			var backdrop = null;
			
			var highlightString = adaptParent.$(".narrative-mobile-holder").css("background-color");
			highlightString = highlightString.substring(4);
			var highlightArray = highlightString.split(/, /);
			
			var highlightColor = {
				r: parseInt(highlightArray[0])/255,
				g: parseInt(highlightArray[1])/255,
				b: parseInt(highlightArray[2])/255
			}
			
			// Make all objects clickable.
			object.traverse( function(child) {
				if (child instanceof THREE.Mesh) {
					
					// Shadow.
					var separators = ['.', '_'];
					var nameArr = child.name.split(new RegExp('[' + separators.join('') + ']', 'g'));
					child.castShadow = true;
					/*if (nameArr[0] != "receiveShadow")*/ child.receiveShadow = true;
					
					// Increase line thickness on portable devices.
					var lineWidth = (isMobile) ? 0.99 : 0.5;
					
					// Add hotspots.
					if (nameArr[0] == "hotspot")
					{
						// Start each child with a red outline.
						setTimeout(function(){child.redOutline = createObjectOutline(child, 209/255, 95/255, 95/255, lineWidth);}, 1, 0);
						child.visited = false;
						
						child.onClick = function(){
							
							// Wait for animations to finish before you show more content.
							if (adaptParent.$el.find("*:animated").length > 0) {return;}
							
							removeOutline(child.redOutline); // Remove the red outline from this object.
							removeOutline(outlineObject); // Unhilight the previously highlighted object (if there was one).
							
							setTimeout(function(){
								if (child.greenOutline == undefined) child.greenOutline = createObjectOutline(child, 149/255, 234/255, 149/255, (lineWidth/2), 0);
								outlineObject = createObjectOutline(child, highlightColor.r, highlightColor.g, highlightColor.b, (lineWidth*1.5), -100);
							},1);
							
							child.visited = true; // Used by Adapt to determine whether the screen is complete.
							
							// Check if the screen has been complete after each new click.
							var screenComplete = true;
							for (var i = 0; i < hotspots.length; i++) {if (!hotspots[i].visited) {screenComplete = false; break;}}
							if (screenComplete) {console.log("COMPLETE"); onScreenComplete();}
							
							child.badge = true;
							showContent(nameArr[1]);
						}
						hotspots.push(child);
					}
					
					// Draw the backdrop as last as possible.
					if (nameArr[0] == "backdrop")
					{
						//child.material.depthTest = false;
						//child.material.depthWrite = false;
						child.renderOrder = -10;
						backdrop = child;
					}
					
					// Materials.
					if (child.material.materials != undefined) {
						for (var i = 0; i < child.material.materials.length; i++) {
							
							if (child.material.materials[i].name == "glass") {setMaterialToSilver(child.material.materials[i], 0.4); child.renderOrder = 1000;}
							if (child.material.materials[i].name == "silver") {setMaterialToSilver(child.material.materials[i], 0.05);}
							if (child.material.materials[i].name == "super_silver") {setMaterialToSilver(child.material.materials[i], 0.2);}
							if (child.material.materials[i].name == "dark_silver") {setMaterialToSilver(child.material.materials[i], 0.2);}
						}
					}
					else
					{
						if (child.material.name == "glass") {setMaterialToSilver(child.material, 0.4); child.renderOrder = 1000;}
						if (child.material.name == "silver") {setMaterialToSilver(child.material, 0.05);}
						if (child.material.name == "super_silver") {setMaterialToSilver(child.material, 0.2);}
						if (child.material.name == "dark_silver") {setMaterialToSilver(child.material, 0.2);}
					}
				}
			});
			
			//object.remove(backdrop);
			//ddv.addToSceneBG(backdrop);
			ddv.addToScene(object);
			
			// Finished loading, show the 3D viewer!
			adaptParent.$(".holder-3d-placeholder").hide();
		});
		
		var me = this;
		
		// Variable setup.
		this.CAMERA_HEIGHT = 80;
		
		ddv.initOrbitControls();
		
		ddv.setStepAction(function () {

			// Check if hotspots are moused over.
			/*activeHotspot = null;
			jQuery.each(hotspots, function(i, e){
				
				if (checkHotspotRaycast(ddv, e))
				{
					// Record which hotspot is active.
					activeHotspot = e;
				}
			});*/
			
			// Hide the content if the user pans the camera over a certain amount.
			if (ddv.tempCameraPos != null)
			{
				//console.log(ddv.tempCameraPos.clone().sub(ddv.camera.position).length());
				
				if (ddv.tempCameraPos.clone().sub(ddv.camera.position).length() > ((isMobile) ? 50 : 20))
				{
					if (adaptParent.$(":animated").length == 0)
					{
						hideContent(adaptParent.$(".narrative-3d-reveal"));
						ddv.tempCameraPos = null;
					}
				}
			}
		});
		
		ddv.setMouseDownAction(function () {
			
			// Check for clicks on outlined areas.
			activeHotspot = null;
			
			var dist = 9999;
			
			jQuery.each(hotspots, function(i, e){
				
				var rc = checkHotspotRaycast(ddv, e);
				
				//console.log(rc);
				
				if (rc >= 0 && rc < dist)
				{
					dist = rc;
					
					// Record this hotspot as active (will be changed if a closer one is found).
					activeHotspot = e;
				}
			});
			
			if (activeHotspot != null)
			{
				activeHotspot.onClick();
			}
		});
	}
	
	function initRaycast(object)
	{
		var intersect = ddv.getRaycastHit(object);
		if (intersect.length != 0) {
		
			//console.log(object.name);
		}
	}
	
	this.remove = function()
	{
		hideContent(adaptParent.$(".narrative-3d-reveal")); // Hide content on screen transition to be on the safe side.
		adaptParent.animateHideContent();
		
		// Prevents a minor bug on mobile devices caused by the screen freezing while the model viewer unloads to load the next one.
		adaptParent.$(".clearfix").finish().css("height", 0);
		
		ddv.setMouseDownAction(null);
		ddv.setStepAction(null);
		ddv.remove();
	}
	
	function centerObjectOrigin(object)
	{
		var bb = new THREE.BoundingBoxHelper(object, 0xff0000);
		bb.update();
		object.geometry.applyMatrix( new THREE.Matrix4().makeTranslation(-bb.position.x, -bb.position.y, -bb.position.z) );
		object.position.x = bb.position.x;
		object.position.y = bb.position.y;
		object.position.z = bb.position.z;
	}
	
	function setMaterialToSilver(material, reflectivity)
	{
		material.envMap = reflectionCube;
		material.reflectivity = reflectivity;
		material.emissive = new THREE.Color( 0.1, 0.1, 0.1 );
		material.premultipliedAlpha = true;
		material.alphaTest = 0.05;
	}
	
	function checkHotspotRaycast(ddv, hotspot) {
		
		var intersect = ddv.getRaycastHit(hotspot);
		if (intersect.length != 0) {
			
			// Mouse over.
			//this.hotspotOver = hotspot;
			//mouseOverHotspot(ddv, this.hotspotOver, intersect[0].point);
			
			return intersect[0].distance; // We want to select the one closest to the cursor.
		}
		
		return -1;
	}
	
	function mouseOverHotspot(ddv, hotspot, point)
	{
		// Change the cursor to an "action" cursor.
		this.cursor.material.map = this.cursor.textures.texEyes;
		this.cursor.position.set(point.x, point.y, point.z);
		this.cursor.lookAt(ddv.camera.position);
		this.cursor.scale.set(2,2,2);
		this.cursor.visible = true;
			
		// Store which object we're looking at.
		//this.cursor.target = intersect[0].object;
		
		if (hotspot.noMouseOver != undefined) return;
		
		ddv.scaleSmooth(hotspot, HOTSPOT_SIZE*1.5, 0.25);
	}
	function mouseOutHotspot(ddv, hotspot)
	{
		if (hotspot.noMouseOver != undefined) return; 
		
		ddv.scaleSmooth(hotspot, HOTSPOT_SIZE, 0.25);
	}
	
	function createObjectOutline(obj, r, g, b, thickness, depth) {
	
		var geometryClone = obj.geometry;
		var outlineMesh1 = new THREE.Mesh( geometryClone, createOutlineMaterial(0, 0, 0, 1, 0.001) );
		var outlineMesh2 = new THREE.Mesh( geometryClone, createOutlineMaterial(r, g, b, 1, thickness) );
		
		outlineMesh1.renderOrder = depth;
		outlineMesh2.renderOrder = depth;
		
		var o = {outlineMask: outlineMesh1, outlineColor: outlineMesh2};
		o.parent = obj;
		
		obj.add( outlineMesh1 );
		outlineMesh1.applyMatrix( outlineMesh1.parent.matrixWorld );
		obj.remove(outlineMesh1);
		
		obj.add( outlineMesh2 );
		outlineMesh2.applyMatrix( outlineMesh2.parent.matrixWorld );
		obj.remove(outlineMesh2);
		
		return ddv.addOutline(o);
	}
	
	function removeOutline(outline) {
		// Hide the old outline (if it exists).
		if (outline != null)
		{
			ddv.removeOutline(outline);
			outline = null;
		}
	}
}