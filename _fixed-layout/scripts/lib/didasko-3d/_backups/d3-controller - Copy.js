/**************************/
/* 3D Model viewer script */
/**************************/
/* Author: Milton Plotkin
/* Date: September 20th 2016
/* Function: Script to be loaded externally by Adobe Edge that will create a ThreeJS canvas on the screen.
/* Will load a specific .obj model and its .mtl, and will allow basic mouse/touch orbitControls to rotate/zoom it around.
/* Will also allow for hotspots to be clicked to reveal information on the object. */

function D3Controller(scriptdir, containerElement, isDetailed) {

	var me = this;

	// Load in dependencies (important to load Three.js first).
	$.getScript(scriptdir + 'lib/threejs/Three.js', function() {
		$.when(
			$.getScript(scriptdir + "../greensock/TweenLite.min.js" ),
			$.getScript(scriptdir + "lib/threejs/OBJLoader.js" ),
			$.getScript(scriptdir + "lib/threejs/MTLLoader.js" ),
			$.getScript(scriptdir + "lib/threejs/OBJMTLLoader.js" ),
			$.getScript(scriptdir + "lib/threejs/OrbitControls.js" ),
			$.getScript(scriptdir + "lib/d3-fps.js" ),
			$.getScript(scriptdir + "lib/d3-orbit.js" )
		).done(function(){

			console.log("d3-controller.js: External scripts loaded.");
			me.init3D();
		});
	});

	/*************/
	/* VARIABLES */
	/*************/

	var id;
	
	this.allLoaded = false;

	this.onProgress;
	this.onError;

	this.loadMng;
	this.objLoader = null;
	this.mtlLoader;
	this.jsonLoader;

	this.$containerElement;

	this.camera;
	this.scene;
	this.composer;
	this.renderer;

	this.raycaster;
	this.isTouchActive = false;
	
	this.ddfps;
	this.ddorbit;

	this.onMouseDownAction = function () {};
	this.onMouseUpAction = function () {};
	this.onStepAction = function () {};
	
	this.onCameraMoveComplete = function () {};

	this.outlines;
	
	//this.mousePos = new THREE.Vector2(0, 0);

	this.stopClick = false; // Used to only detect only the first object clicked on.

	this.LOW_RES = false;

	// Animation variables.
	this.animDelay = {

		enabled : false,
		onComplete : null
	} // Delay actions until animations finish playing.
	this.clock;
	this.mixer;
	this.action;

	this.billboards; // Custom billboards (all objects passed into this will face cam).
	
	this.mouse = {
		x : 0,
		y : 0
	};
	this.mouseChange = {
		x : 0,
		y : 0
	};
	
	var referenceToMe;
	var referenceToResizeFunction;

	/*******************/
	/* Event listeners */
	/*******************/

	function onMouseDown(me) {

		if (me.onMouseDownAction != null) {
			me.onMouseDownAction();
		}
	}

	function onMouseUp() {}

	// Update the stored mouse position variable every time the user moves the pointer.
	function onMouseMove(eX, eY, me, isTouch) {

		var x = ((eX - me.$containerElement.offset().left) / me.$containerElement.width()) * 2 - 1;
		var y =  - ((eY - me.$containerElement.offset().top + ((isTouch) ? 0 : $(window).scrollTop())) / me.$containerElement.height()) * 2 + 1;

		me.mouseChange.x = me.mouse.x - x;
		me.mouseChange.y = me.mouse.y - y;

		me.mouse.x = x;
		me.mouse.y = y;
		
		//console.log("y: " + (- ((eY - me.$containerElement.offset().top) / me.$containerElement.height()) * 2 + 1));
		
		// Move the cursor to the mouse.
		$("#cursor").css({"left":eX-me.$containerElement.offset().left, "top":eY-me.$containerElement.offset().top});
	}

	/***************/
	/* SCENE SETUP */
	/***************/

	this.isPortableDevice = function() {
		return (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent));
	}
	
	this.addBillboard = function (billboard) {
	
		this.billboards.push(billboard);
	}

	// Resets the camera position.
	this.resetCamera = function () {
		TweenLite.killTweensOf(this.camera.position);
		
		this.camera.position.x = 0;
		this.camera.position.y = 80;
		this.camera.position.z = -80;
		
		this.onCameraMoveComplete();
	}
	
	this.init3D = function () {

		this.clock = new THREE.Clock();
	
		this.billboards = new Array();
		
		this.outlines = new Array(); // Stores scenes for use with outlines.
	
		// Load a lower poly Motherboard if the user is on mobile Safari or android.
		// Note that certain devices cannot handle high resolution images (4096+).
		this.LOW_RES = this.isPortableDevice();

		// Square on the field that the THREE.js will render to.
		this.$containerElement = containerElement;
		
		this.scene = new THREE.Scene();

		// Camera.
		this.camera = new THREE.PerspectiveCamera(65, this.$containerElement.width() / this.$containerElement.height(), 0.1, 2000);
		this.camera.lookAt(this.scene.position);

		this.resetCamera();
		
		// Ensure consistant movement (based off the device clock).
		this.clock = new THREE.Clock();

		// Render the scene with a transparent background (so that the designer can put their own in if they choose.
		this.renderer = new THREE.WebGLRenderer({
				alpha : true,
				antialias : true,
				sortObjects : true
			});
		this.renderer.setSize(this.$containerElement.width(), this.$containerElement.height());
		this.renderer.autoClear = false; // Allow for layered scenes.

		if (isDetailed)
		{
			console.log("Shadows enabled");
			this.renderer.shadowMap.enabled = true;
			this.renderer.shadowMap.type = THREE.PCFSoftShadowMap;
		}
		
		// Store a reference to our 3D container.
		this.$containerElement[0].appendChild(this.renderer.domElement);

		/* Raycasting */
		this.raycaster = new THREE.Raycaster();
		
		// Initiate the lights.
		this.initLights();

		// Initiate loaders.
		this.initLoaders();

		// Initiate event listeners.
		var me = this;
		$(document).bind('mousedown', function (event) {
			event.preventDefault();
			
			onMouseDown(me);
		});
		$(document).bind('touchstart', function (event) {
			//event.preventDefault();
			
			// Call once to ensure the cursor is on the screen.
			onMouseMove(event.originalEvent.touches[0].pageX, event.originalEvent.touches[0].pageY, me, true);
			
			onMouseDown(me);
		});
		$(document).bind('mousemove', function (event) {
			onMouseMove(event.clientX, event.clientY, me, false);
		});
		$(document).bind('touchmove', function (event) {
			onMouseMove(event.originalEvent.touches[0].pageX, event.originalEvent.touches[0].pageY, me, true);
		});
		$(document).bind('mouseup', function (event) {
			
			event.preventDefault();
			
			if (me.ddfps && me.ddfps.canMouseUp) {
			    me.onMouseUpAction();
			}
		});
		$("canvas").bind('touchend', function (event) {
			
			event.preventDefault();
			
			//me.$containerElement.hide();
			
			if (me.ddfps && me.ddfps.canMouseUp) {
			    me.onMouseUpAction();
			}
		});
		
		referenceToMe = this;
		referenceToResizeFunction = referenceToMe.onWindowResize;
		
		window.addEventListener( 'resize', referenceToResizeFunction, false );

		// Run the render each step.
		this.animate3D();
		
		// Resize the screen upon loading.
		$(document).ready(referenceToResizeFunction);
	}

	// If called, will initiate an FPS view with click+drag controls.
	this.initFPSControls = function () {
	
		this.ddfps = new DDFPSControls();
		this.ddfps.init(this.$containerElement, this.camera);
	}
	
	this.initOrbitControls = function () {
	
		this.ddorbit = new DDOrbitControls();
		this.ddorbit.init(this.$containerElement, this.camera);
		
		/*var me = this;
		me.render();
		this.ddorbit.getOControls().addEventListener( 'change', me.render );*/
	}

	this.addToScene = function (object) {
		this.scene.add(object);
	}
	
	this.removeFromArray = function (array, element) {
	
		for(var i = array.length - 1; i >= 0; i--) {
			if(array[i] === element) {
			   array.splice(i, 1);
			}
		}
	}
	
	this.addOutline = function (outlineObject) {
		
		var outline = {
			sceneMask: new THREE.Scene(),
			sceneOutline: new THREE.Scene()
		}
		
		outline.sceneMask.add(outlineObject.outlineMask);
		outline.sceneOutline.add(outlineObject.outlineColor);
		
		//this.scene.add(outlineObject.outlineMask);
		//this.scene.add(outlineObject.outlineColor);
		
		this.outlines.push(outline);
		
		return outline;
	}
	this.removeOutline = function (outlineObject) {
		
		this.removeFromArray(this.outlines, outlineObject);
	}
	
	this.removeFromScene = function (object) {
		this.scene.remove(object);
	}
	
	this.setMouseDownAction = function (action) {
		this.onMouseDownAction = action;
	}
	
	this.setMouseUpAction = function (action) {
		this.onMouseUpAction = action;
	}

	this.setStepAction = function (action) {
		this.onStepAction = action;
	}

	/************/
	/* LIGHTING */
	/************/

	this.initLights = function () {

		// Less lighting improves performance.
		this.scene.add(new THREE.AmbientLight(0xaaaaaa));

		/*if (this.LOW_RES) {

			var l1 = new THREE.DirectionalLight(0xffffff, 0.8);
			l1.position.set(100, 400, -50);
			this.scene.add(l1);

			var l2 = new THREE.DirectionalLight(0xffffff, 0.6);
			l2.position.set(-50, 100, 30); // Intensity of these numbers affects how intense the shadow map will be.
			this.scene.add(l2);

			enableLightShadow(l1, this);
			enableLightShadow(l2, this);

		} else {*/
		
		// Changed the lighting to be more basic on all devices. Shadows removed for performance.
		
		var l1 = new THREE.DirectionalLight(0xffffff, 1);
		l1.position.set(0.5, 50, 0.5);
		
		/*var l2 = new THREE.DirectionalLight(0xffffff, 1);
		l2.position.set(-0.5, -1, -1); // Intensity of these numbers affects how intense the shadow map will be.
		this.scene.add(l2);*/
		
		this.scene.add(l1);
		
		//}
	}
	
	function enableLightShadow(light, me)
	{
		light.castShadow = true;
		//me.scene.add(new THREE.CameraHelper( light.shadow.camera ));
		
		light.shadow.mapSize.width = 256; // The lower these numbers, the more blurred the shadow.
		light.shadow.mapSize.height = 256;
		light.shadowMapBias = 0.001;
		
		var d = 1000;

		light.shadow.camera.left = -d;
		light.shadow.camera.right = d;
		light.shadow.camera.top = d;
		light.shadow.camera.bottom = -d;

		light.shadow.camera.far = 1000;
	}

	this.isOffScreen = function () {
		
		//if (this.$containerElement == undefined) {console.log("WHAT?"); return false;}
		var top = referenceToMe.$containerElement.offset().top;
		var height = referenceToMe.$containerElement.outerHeight();
		var y = $(window).scrollTop();

		return (y+$(window).outerHeight() < top || y > top+height);
	}
	
	this.animate3D = function () {
	
		id = requestAnimationFrame(this.animate3D.bind(this));
		this.render();
	}

	this.renderOutlines = function() {
		
		var gl = this.renderer.domElement.getContext('webgl') || this.renderer.domElement.getContext('experimental-webgl');
		
		for (var i = 0; i < this.outlines.length; i++)
		{
		
			//http://stackoverflow.com/questions/26134077/passing-color-to-fragment-shader-from-javascript
			
			// Make the entire stencil solid first.
			gl.clearStencil(1);
			gl.clear(gl.STENCIL_BUFFER_BIT);
			
			// Whenever the value of a pixel is 1 in the mask, make that pixel = 0 in the stencil.
			// 0xFF is the identifier I think.
			gl.stencilFunc(gl.ALWAYS, 0, 0xFF);
			gl.stencilOp(gl.REPLACE, gl.GL_REPLACE, gl.GL_REPLACE);
			
			// Disable color (u can also disable here the depth buffers)
			gl.colorMask(false, false, false, false);

			// Write to stencil
			gl.enable(gl.STENCIL_TEST);
			
			gl.stencilMask(0xFF);
			
			// Mask (Create a stencil that we render the next pass into)
			this.renderer.render(this.outlines[i].sceneMask, this.camera);
			
			// Wherever the stencil is = 1, show the model.
			gl.stencilFunc(gl.EQUAL, 1, 0xFF);
			gl.stencilOp(gl.KEEP, gl.KEEP, gl.KEEP);
			
			// Enable color
			gl.colorMask(true, true, true, true);
			
			// Clear depth buffer (seems important)
			//this.renderer.clearDepth();
			this.renderer.render(this.outlines[i].sceneOutline, this.camera);
			
			// Disable stencil test;
			gl.disable(gl.STENCIL_TEST);
		
			this.outlines[i].sceneMask.updateMatrixWorld();
			this.outlines[i].sceneOutline.updateMatrixWorld();
		}
		
		
		//this.renderer.clearDepth();
		//this.renderer.render(this.sceneMask, this.camera);
	}
	
	/*this.renderOutlinesBackup = function() {
		
		var gl = this.renderer.domElement.getContext('webgl') || this.renderer.domElement.getContext('experimental-webgl');
		
		// Make the entire stencil solid first.
		gl.clearStencil(1);
		gl.clear(gl.STENCIL_BUFFER_BIT);
		
		// Whenever the value of a pixel is 1 in the mask, make that pixel = 0 in the stencil.
		// 0xFF is the identifier I think.
		gl.stencilFunc(gl.ALWAYS, 0, 0xFF);
		gl.stencilOp(gl.REPLACE, gl.GL_REPLACE, gl.GL_REPLACE);
		
		// Disable color (u can also disable here the depth buffers)
		gl.colorMask(false, false, false, false);

		// Write to stencil
		gl.enable(gl.STENCIL_TEST);
		
		gl.stencilMask(0xFF);
		
		// Mask (Create a stencil that we render the next pass into)
		this.renderer.render(this.sceneMask, this.camera);
		
		// Wherever the stencil is = 1, show the model.
		gl.stencilFunc(gl.EQUAL, 1, 0xFF);
		gl.stencilOp(gl.KEEP, gl.KEEP, gl.KEEP);
		
		// Enable color
		gl.colorMask(true, true, true, true);
		
		// Clear depth buffer (seems important)
		this.renderer.clearDepth();
		this.renderer.render(this.sceneOutline, this.camera);
		
		// Disable stencil test;
		gl.disable(gl.STENCIL_TEST);
	}*/ //http://stackoverflow.com/questions/43730580/three-js-webgl-use-black-and-white-scene-as-a-mask-to-make-an-outline
	
	this.render = function () {

		// Detect if the viewer is too far off screen and stop rendering it.
		if (this.isOffScreen()) {return;}
	
		var me = referenceToMe;
	
		me.renderer.clear();
		me.renderer.render(me.scene, me.camera);
		me.renderOutlines();
		
		var delta = me.clock.getDelta();
		var theta = me.clock.getElapsedTime();
		
		//this.renderer.clear();
		//this.composer.render(delta);

		if (me.onStepAction)
			me.onStepAction();

		// MouseChange needs to be set back to 0 between frames (as there's no event to check if the mouse is stationary).
		// This is likely a temporary solution.
		me.mouseChange.x = 0;
		me.mouseChange.y = 0;
		
		var me = me;
		
		me.scene.updateMatrixWorld();
		
		jQuery.each(me.billboards, function(i, e){
			
			var v = new THREE.Vector3(e.position.x-me.camera.getWorldDirection().x, e.position.y, e.position.z-me.camera.getWorldDirection().z);
			//var v = new THREE.Vector3(me.camera.position.x, e.position.y, me.camera.position.z);
			e.lookAt(v);
			
			//console.log(e.position.x + " " + e.position.y + " " + e.position.z);
			
			//e.rotation.y = Math.atan2( ( me.camera.position.x - e.position.x ), ( me.camera.position.z - e.position.z ) );
			
			//console.log(me.camera.position);
			
			//e.rotation.setRotationFromMatrix( me.camera.matrix );
		});
		
		if (me.mixer) {
			me.mixer.update(delta);
		}
		
		if (me.ddorbit) me.ddorbit.update();
	}

	/***********/
	/* LOADING */
	/***********/

	this.initLoaders = function () {

		var me = this;

		// Show a loading bar until everything has been prepared.
		//this.$containerElement.hide();
		var loadingBar = $("#Stage_LOADINGBAR");

		this.loadMng = new THREE.LoadingManager();
		this.onProgress = function (item, loaded, total) {
			console.log(item, loaded, total);
		};
		this.loadMng.onLoad = function () {

			console.log("- All content loaded -");
			$("#Stage_PRELOADER").hide();
			me.$containerElement.show();
		};

		// Shows loading status.
		this.onProgress = function (xhr) {
			if (xhr.lengthComputable) {
				var percentComplete = xhr.loaded / xhr.total * 100;
				loadingBar.outerWidth(percentComplete * 3);

				console.log(Math.round(percentComplete, 2) + '% downloaded');
			}
		};

		// Informs of errors.
		onError = function (xhr) {};

		this.objLoader = new THREE.OBJLoader(this.loadMng);
		this.mtlLoader = new THREE.MTLLoader();

		this.jsonLoader = new THREE.JSONLoader();

		console.log("loaders are done");
	}

	this.loadModel = function (path, modName, onLoad) {

		var me = this;

		this.mtlLoader.setPath(path + "\\");
		this.mtlLoader.load(modName + ".mtl", function (materials) {

			materials.preload();
			
			// Allow for transparency on all textures.
			for (var mat in materials.materials)
			{
				materials.materials[mat].transparent = true;
			}

			me.objLoader.setMaterials(materials);
			me.objLoader.setPath(path + "\\");
			me.objLoader.load(modName + ".obj", function (object) {

				onLoad(object);

				//materials[i].side = THREE.DoubleSide;

			}, this.onProgress, this.onError);

		});
	}

	this.onHotspotMouseEnter = function (hitbox) {
		hitbox.hovered = true;
	}
	this.onHotspotMouseExit = function (hitbox) {
		hitbox.hovered = false;
	}

	this.moveCamera = function (pos, onCompleteAction) {

	    var me = this;
	    me.ddfps.disable();

	    me.onCameraMoveComplete = function () { me.ddfps.enable(); onCompleteAction(); };
		if (me.onCameraMoveComplete == null)
			me.onCameraMoveComplete = function () {};

		TweenLite.to(this.camera.position, 1, {
			x : pos.x,
			y : pos.y,
			z : pos.z,
			onComplete : me.onCameraMoveComplete
		});
	}
	this.scaleSmooth = function (object, scale, time) {
		
		TweenLite.to(object.scale, time, {
			x : scale,
			y : scale,
			z : scale
		});
	}
	
	this.rotateCamera = function (rot) {

		TweenLite.to(this.camera.rotation, 1, {
			x : convertToRad(rot.x),
			y : convertToRad(rot.y),
			z : convertToRad(rot.z)
		});
	}

	this.moveObject = function (object, pos) {

		TweenLite.to(object.position, 1, {
			x : pos.x,
			y : pos.y,
			z : pos.z
		});
	}

	function convertToRad(val) {
		return val * Math.PI / 180;
	}

	// Check if the raycast (mouse) hit the selected object.
	this.getRaycastHit = function (object) {

		// Update the picking ray with the camera and mouse position
		this.raycaster.setFromCamera(this.mouse, this.camera);

		// calculate objects intersecting the picking ray
		var intersect = this.raycaster.intersectObject(object, false);

		return intersect;
	}

	this.loadAnimatedModel = function (path) {

		var me = this;
	
		this.jsonLoader.load(path, function (geometry, materials) {

			materials.forEach(function (material) {
				material.skinning = true;
			});

			var mesh = new THREE.SkinnedMesh(geometry, new THREE.MeshFaceMaterial(materials));
			me.addToScene(mesh);

			// Actions
			mixer = new THREE.AnimationMixer(mesh);
			me.readAnimatedActions(mesh, geometry.animations);
		});
	}

	this.readAnimatedActions = function (mesh, anim) {
		// Record actions.
		mesh.actions = new Array();

		for (var i = 0; i < anim.length; i++) {
			var nameSplit = anim[i].name.split("_");

			// If this isn't a "swim" animation, don't read it (Blender sometimes exports extra, generated animations).
			//if (nameSplit[0] != "swim")	continue;

			//var group = nameSplit[1];

			// Create a new array to store animation groups (example: all animations dictating arm movements) in the same variable.
			//if (action[group] == undefined) action[group] = new Array();

			// And then, add the animation to the array.
			var a = mixer.clipAction(anim[i]);
			a.setEffectiveWeight(1).stop();
			mesh.actions.push(a);

			// And also log it for debug.
			console.log("Added " + anim[i].name);

			// Temporary: populate a list of action controllers.
			//$("#temp-list").append("<li id='"+i+"-temp-list'>"+anim[i].name+"</li>");

			setTimeout(function(){this.fadeAction(mesh, anim[i].name, i);}, 500);

			//initActionClick($("#"+i+"-temp-list"), group, action[group].length-1);
		}
	}
	
	// Allow for resizing of the 3D view in realtime!
	this.onWindowResize = function(){
	
		var me = referenceToMe;
	
		setTimeout(function(){
			
			var windowHeight = $("body").outerHeight();
			var headerHeight = $("#id-header").outerHeight();
			var footerHeight = $("footer").outerHeight();
			
			//console.log("Header: " + headerHeight);
			//console.log("Footer: " + footerHeight);
			
			// Resize the container element (needed a JavaScript solution).
			//me.$containerElement.css({"height": windowHeight-footerHeight-headerHeight});
									  
			//console.log("new height: " + me.$containerElement.outerHeight());
			
			me.camera.aspect = me.$containerElement.outerWidth() / me.$containerElement.outerHeight();
			me.camera.updateProjectionMatrix();

			me.renderer.setSize( me.$containerElement.outerWidth(), me.$containerElement.outerHeight() );
		},100);

	}

	this.remove = function () {
		
		cancelAnimationFrame(id);
		this.renderer.domElement.addEventListener('dblclick', null, false); //remove listener to render
		//this.scene = null;
		//this.projector = null;
		this.camera = null;
		//this.controls = null;
		this.$containerElement.empty();
		this.raycaster = null;
		
		window.removeEventListener( 'resize', referenceToResizeFunction, false);
	}
	
	this.fadeAction = function () {

		var activeActionName = 'swim1';

		return function (mesh, name, index) {

			if (activeActionName === name) {
				return;
			}

			// Fade out all actions that aren't this one.
			for (var i = 0; i < mesh.actions.length; i++) {
				if (i == index)
					continue;
				
				mesh.actions[i].fadeOut(.3);
			}

			//var f = action[group][index].play();
			var t = mesh.actions[index].play();
			t.setEffectiveWeight(1);

			//f.enabled = true;
			t.enabled = true;

			//f.crossFadeTo( t, .3 );
			//f.fadeOut(.3);
			t.fadeIn(.3);

			activeActionName = name;
		}

	}
	();
}