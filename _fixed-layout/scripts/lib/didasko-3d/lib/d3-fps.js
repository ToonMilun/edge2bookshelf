/**********************************************************************************************/
/* FPS view with click+drag controls (modified version of Threejs's equirectangular panorama) */
/**********************************************************************************************/
/* Author: Threejs, Milton Plotkin
/* Date: February 2th 2017 */

function DDFPSControls() {
	
	this.isUserInteracting = false;

	this.enabled = true; // If set to false, will not execute any of its functions, leaving the camera as is.

	this.canMouseUp = true; // Is set to false if you click and drag.
	
	this.$container;
	this.camera;
	
	this.init = function (container, camera) {
	
		this.$container = container;
		this.camera = camera;
		this.camera.target = new THREE.Vector3(360, 80, -80);
	
		this.camera.fov = 60;
	
		var me = this;
		var onPointerDownPointerX, onPointerDownPointerY, onPointerDownLon, onPointerDownLat;
	
		var lat = 0;
		var lon = 0;
		var phi = 0;
		var theta = 0;

		mouse = {
		    x: 0,
		    y: 0
		};

		$(document).on('mousedown touchstart', function (event) {
		
		    if (!me.enabled) return;

		    //event.preventDefault();

			me.isUserInteracting = true;

			onPointerDownPointerX = event.clientX;
			onPointerDownPointerY = event.clientY;

			// If on tablet.
			if (isNaN(onPointerDownPointerX) || isNaN(onPointerDownPointerY))
			{
				onPointerDownPointerX = event.originalEvent.touches[0].pageX;
				onPointerDownPointerY = event.originalEvent.touches[0].pageY;
			}
			
			onPointerDownLon = lon;
			onPointerDownLat = lat;
			
			me.canMouseUp = true;
			
		});
		
		var mouseMove = function(clientX, clientY) {
			if (!me.enabled) return;
			
			if (isNaN(clientX) || isNaN(clientY)) return;
			
			if (me.isUserInteracting === true) {

				lon = (onPointerDownPointerX - clientX) * 0.1 + onPointerDownLon;
				lat = (clientY - onPointerDownPointerY) * 0.1 + onPointerDownLat;
			}
			
			var x = ((clientX - me.$container.offset().left) / me.$container.width()) * 2 - 1;
			var y =  - ((clientY - me.$container.offset().top) / me.$container.height()) * 2 + 1;
			
		    // Check to see if the user has dragged. If they have, then do not count this as a click.
			if (Math.abs(mouse.x - x) > 0.001 || Math.abs(mouse.y - y) > 0.001) me.canMouseUp = false;

			mouse.x = x;
			mouse.y = y;
			
			if (mouse.x > 1 || mouse.x < -1 || mouse.y > 1 || mouse.y < -1) return;
			
			lat = Math.max( - 85, Math.min(85, lat));
			phi = THREE.Math.degToRad(90 - lat);
			theta = THREE.Math.degToRad(lon);
			
			me.camera.target.x = me.camera.position.x+500 * Math.sin(phi) * Math.cos(theta);
			me.camera.target.y = me.camera.position.y+500 * Math.cos(phi);
			me.camera.target.z = me.camera.position.z+500 * Math.sin(phi) * Math.sin(theta);

			me.camera.lookAt(me.camera.target);
		}
		
		$(document).on('mousemove', function (event) {	
		    mouseMove(event.clientX, event.clientY);
		});
		$(document).on('touchmove', function (event) {	
		
			event.preventDefault();
		
		    mouseMove(event.originalEvent.touches[0].pageX, event.originalEvent.touches[0].pageY);
		});
		
		// Move the view on page load to face forward.
		$(document).ready(function () {	
		
			me.camera.lookAt(me.camera.target);
			mouseMove(0, 0);
		});
		
		$(document).on('mouseup touchend', function (event) {
			
		    if (!me.enabled) return;

			me.isUserInteracting = false;
			
		});
		/*document.addEventListener('wheel', function (event) {
			
		    if (!me.enabled) return;

			me.camera.fov += event.deltaY * 0.05;
			me.camera.updateProjectionMatrix();
			
		}, false);*/
	}

	this.disable = function () {
	    this.isUserInteracting = false;
	    this.enabled = false;
	}
	this.enable = function () {
	    this.enabled = true;
	}
}