/**************************/
/* 3D Model viewer script */
/**************************/
/* Author: Milton Plotkin
/* Date: September 20th 2016
/* Function: Script to be loaded externally by Adobe Edge that will create a ThreeJS canvas on the screen.
/* Will load a specific .obj model and its .mtl, and will allow basic mouse/touch orbitControls to rotate/zoom it around.
/* Will also allow for hotspots to be clicked to reveal information on the object. */

function D3Controller(scriptdir, $container, onReady) {

	var me = this;

	// Load in dependencies (important to load Three.js first).
	$.getScript(scriptdir + 'lib/threejs/Three.js', function() {
		$.when(
			$.getScript(scriptdir + "../greensock/TweenLite.min.js" ),
			$.getScript(scriptdir + "lib/threejs/OBJLoader.js" ),
			$.getScript(scriptdir + "lib/threejs/MTLLoader.js" ),
			$.getScript(scriptdir + "lib/threejs/OBJMTLLoader.js" ),
			$.getScript(scriptdir + "lib/threejs/OrbitControls.js" ),
			$.getScript(scriptdir + "lib/d3-fps.js" ),
			$.getScript(scriptdir + "lib/d3-orbit.js" )
		).done(function(){

			console.log("External scripts loaded.");
			init();
		});
	});

	/*************/
	/* VARIABLES */
	/*************/

	var id;
	
	var RENDER_QUALITY_LOW = false; // Set to true depending on the device being used.
	
	var billboardObjectArray; 		// All object in this array will face the camera at all times.
	
	// ThreeJS //
	var camera;
	var scene;
	var composer;
	var renderer;
	
	var clock;
	
	var raycaster;
	
	var loadMng;
	var objLoader = null;
	var mtlLoader;
	var jsonLoader;
	
	var allLoaded = false;
	var onProgress;
	var onError;
	
	var d3orbit;
	
	var mousePos;
	var mouseMove;
	var mouseDownPos;
	
	var onMouseDownAction = function() {};
	var onMouseClickAction = function() {};
	var onStepAction = function () {};
	
	var reflectionCube;
	
	/*var allLoaded = false;

	var onProgress;
	var onError;

	

	var $containerElement;

	var camera;
	var scene;
	var composer;
	var renderer;

	
	var isTouchActive = false;
	
	var ddfps;
	var ddorbit;

	var onMouseDownAction = function () {};
	var onMouseUpAction = function () {};
	
	var onCameraMoveComplete = function () {};

	var outlines;
	
	//this.mousePos = new THREE.Vector2(0, 0);

	var stopClick = false; // Used to only detect only the first object clicked on.

	// Animation variables.
	var animDelay = {

		enabled : false,
		onComplete : null
	} // Delay actions until animations finish playing.
	var clock;
	var mixer;
	var action;
	
	var mouse = {
		x : 0,
		y : 0
	};
	var mouseChange = {
		x : 0,
		y : 0
	};
	
	var referenceToMe;
	var referenceToResizeFunction;*/

	/**********************/
	/* MOUSE/TOUCH EVENTS */
	/**********************/
	
	function onMouseDown() {

		mouseDownPos.x = mousePos.x;
		mouseDownPos.y = mousePos.y;
	
		if (onMouseDownAction != null) onMouseDownAction();
	}
	
	function onMouseClick() {

		if (onMouseClickAction != null) onMouseClickAction();
	}
	
	this.SetOnMouseDown = function(a){
		onMouseDownAction = a;
	}
	
	this.SetOnMouseClick = function(a){
		onMouseClickAction = a;
	}
	
	// Update the stored mouse position variable every time the user moves the pointer.
	function onMouseMove(eX, eY, isTouch) {

		var x = ((eX - $container.offset().left) / $container.width()) * 2 - 1;
		var y =  - ((eY - $container.offset().top + ((isTouch) ? 0 : $(window).scrollTop())) / $container.height()) * 2 + 1;

		mouseMove.x = mousePos.x - x;
		mouseMove.y = mousePos.y - y;

		mousePos.x = x;
		mousePos.y = y;
	}
	
	/***********/
	/* HELPERS */
	/***********/
	
	this.SetOnStep = function(a){
		onStepAction = a;
	}
	
	// Seperate an object name (seperated by . and _) into an array of tags.
	this.GetNameTags = function(name)
	{
		var separators = ['.', '_'];
		return name.split(new RegExp('[' + separators.join('') + ']', 'g'));
	}
	
	this.GetOrbitControls = function()
	{
		return d3orbit.getOControls();
	}
	
	this.GetCamera = function()
	{
		return camera;
	}
	
	/***************/
	/* SCENE SETUP */
	/***************/

	function isCurrentDevicePortable() {
		return (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent));
	}
	
	/*this.addBillboard = function (billboard) {
	
		billboards.push(billboard);
	}*/

	// Resets the camera position.
	this.ResetCamera = function () {
		TweenLite.killTweensOf(camera.position);
		
		camera.position.x = 0;
		camera.position.y = 80;
		camera.position.z = -80;
		
		//this.onCameraMoveComplete();
	}
	
	function initReflectionCube()
	{
		// Reflective materials [Gold and silver].
		//var path = scriptdir + "media/tex_ref";
		
		// Textures have to be stored locally unfortunately (server doesn't allow them to be read from it).
		var path = "media/tex_ref";
		var format = '.png';
		var urls = [
				path + '2' + format, path + '1' + format,
				path + '3' + format, path + '4' + format,
				path + '5' + format, path + '6' + format
			];

		reflectionCube = new THREE.CubeTextureLoader().load( urls );
	}
	
	function init() {
	
		// Load a lower poly Motherboard if the user is on mobile Safari or android.
		// Note that certain devices cannot handle high resolution images (4096+).
		RENDER_QUALITY_LOW = isCurrentDevicePortable();
		
		billboards = new Array();
		
		// Ensure consistant movement (based off the device clock).
		clock = new THREE.Clock();
		
		// Scene //
		scene = new THREE.Scene();

		// Camera //
		camera = new THREE.PerspectiveCamera(65, $container.width() / $container.height(), 0.1, 2000);
		camera.lookAt(scene.position);

		mousePos = new THREE.Vector2(0, 0);
		mouseMove = new THREE.Vector2(0, 0);
		mouseDownPos = new THREE.Vector2(0, 0);
		
		me.ResetCamera();

		// Render the scene with a transparent background (so that the designer can put their own in if they choose.
		renderer = new THREE.WebGLRenderer({
				alpha : true,
				antialias : true,
				sortObjects : true
			});
		renderer.setSize($container.width(), $container.height());
		renderer.autoClear = false; // Allow for layered scenes.

		$container[0].appendChild(renderer.domElement);
		
		/*if (isDetailed)
		{
			console.log("Shadows enabled");
			renderer.shadowMap.enabled = true;
			renderer.shadowMap.type = THREE.PCFSoftShadowMap;
		}*/
		

		/* Raycasting */
		raycaster = new THREE.Raycaster();
		
		// Initiate the lights.
		initLights();

		// Initiate loaders.
		initLoaders();

		/////////////////////
		// EVENT LISTENERS //
		/////////////////////
		$(document).bind('touchstart', function (event) {
			//event.preventDefault();
			
			// Call once to ensure the cursor is on the screen.
			onMouseMove(event.originalEvent.touches[0].pageX, event.originalEvent.touches[0].pageY, true);
			
			onMouseDown();
		});
		$(document).bind('mousemove', function (event) {
			onMouseMove(event.clientX, event.clientY, false);
		});
		$(document).bind('touchmove', function (event) {
			onMouseMove(event.originalEvent.touches[0].pageX, event.originalEvent.touches[0].pageY, true);
		});
		
		$(document).bind('mousedown', function (event) {
			event.preventDefault();
			
			onMouseDown();
		});
		
		// Bug: if they move their cursor around, then place it right back where they started, it will register a click.
		$(document).bind('mouseup', function (event) {
			
			event.preventDefault();
			
			if (Math.abs(mouseDownPos.x - mousePos.x) < 0.01 &&
				Math.abs(mouseDownPos.y - mousePos.y) < 0.01)
			{
			    onMouseClick();
			}
		});
		$("canvas").bind('touchend', function (event) {
			
			event.preventDefault();
			
			if (Math.abs(mouseDownPos.x - mousePos.x) < 0.01 &&
				Math.abs(mouseDownPos.y - mousePos.y) < 0.01)
			{
			    onMouseClick();
			}
		});
		
		/*
		$(document).bind('mouseup', function (event) {
			
			event.preventDefault();
			
			if (me.ddfps && me.ddfps.canMouseUp) {
			    me.onMouseUpAction();
			}
		});
		$("canvas").bind('touchend', function (event) {
			
			event.preventDefault();
			
			//me.$containerElement.hide();
			
			if (me.ddfps && me.ddfps.canMouseUp) {
			    me.onMouseUpAction();
			}
		});*/
		
		//referenceToMe = this;
		//referenceToResizeFunction = referenceToMe.onWindowResize;
		
		//window.addEventListener( 'resize', referenceToResizeFunction, false );

		// Run the render each step.
		animate3D();
		
		// Resize the screen upon loading.
		//$(document).ready(referenceToResizeFunction);
	}
	
	/***********/
	/* LOADING */
	/***********/

	function initLoadingBar() {
		
		$container.append("<div class='loading-holder center' style='position: absolute; width: 300px; height: 44px;'> \
			<div class='text-h3' style='left: 0px; margin-top: -22px;'> \
				<p>Loading...</p> \
			</div> \
			<div style='position: absolute; left: 0px; top: 50%; width: 100%; height: 12px; border: 2px solid #629bc3;'> \
				<div class='loading-bar' style='background-color: #629bc3; height: 100%; width: 0%;'></div> \
			</div> \
		</div>");
	}
	
	function initLoaders() {

		initLoadingBar();
	
		// Show a loading bar until everything has been prepared.
		//$container.hide();

		loadMng = new THREE.LoadingManager();
		loadMng.onLoad = function () {

			console.log("- All content loaded -");
			$container.find(".loading-holder").hide();
		};

		// Shows loading status.
		onProgress = function (xhr) {
			if (xhr.lengthComputable) {
				var percentComplete = xhr.loaded / xhr.total * 100;

				console.log(Math.round(percentComplete, 2) + '% downloaded');
				$container.find(".loading-bar").css("width", percentComplete + "%");
			}
		};

		// Informs of errors.
		onError = function (xhr) {console.log("An error has occured while loading a model.");};

		objLoader = new THREE.OBJLoader(loadMng);
		mtlLoader = new THREE.MTLLoader();

		jsonLoader = new THREE.JSONLoader();

		console.log("3D Loaders initiated.");
		
		onReady();
	}

	this.LoadModel = function (path, modName, onLoad) {

		mtlLoader.setPath(path + "\\");
		mtlLoader.load(modName + ".mtl", function (materials) {

			materials.preload();
			
			// Allow for transparency on all textures.
			for (var mat in materials.materials)
			{
				materials.materials[mat].transparent = true;
			}

			objLoader.setMaterials(materials);
			objLoader.setPath(path + "\\");
			objLoader.load(modName + ".obj", function (object) {

				onLoad(object);

				//materials[i].side = THREE.DoubleSide;

			}, onProgress, onError);

		});
	}
	
	/************/
	/* LIGHTING */
	/************/

	function initLights() {

		// Less lighting improves performance.
		scene.add(new THREE.AmbientLight(0xaaaaaa));
		
		var l1 = new THREE.DirectionalLight(0xffffff, 0.7);
		l1.position.set(10, 50, 10);
		
		scene.add(l1);
	}
	
	function enableLightShadow(light, me)
	{
		light.castShadow = true;
		//me.scene.add(new THREE.CameraHelper( light.shadow.camera ));
		
		light.shadow.mapSize.width = 256; // The lower these numbers, the more blurred the shadow.
		light.shadow.mapSize.height = 256;
		light.shadowMapBias = 0.001;
		
		var d = 1000;

		light.shadow.camera.left = -d;
		light.shadow.camera.right = d;
		light.shadow.camera.top = d;
		light.shadow.camera.bottom = -d;

		light.shadow.camera.far = 1000;
	}
	
	/*************/
	/* MATERIALS */
	/*************/
	
	this.SetMaterialToReflective = function(material, reflectivity)
	{
		if (reflectionCube == null) initReflectionCube();
		
		material.envMap = reflectionCube;
		material.reflectivity = reflectivity;
		material.emissive = new THREE.Color( 0.1, 0.1, 0.1 );
		material.premultipliedAlpha = true;
		material.alphaTest = 0.05;
	}

	// If called, will initiate an FPS view with click+drag controls.
	this.initFPSControls = function () {
	
		this.ddfps = new DDFPSControls();
		this.ddfps.init(this.$containerElement, this.camera);
	}
	
	this.InitOrbitControls = function (enableZoom) {
	
		d3orbit = new D3OrbitControls(enableZoom);
		d3orbit.init($container, camera);
	}

	this.AddObjectToScene = function (object) {
		scene.add(object);
	}
	
	this.removeFromArray = function (array, element) {
	
		for(var i = array.length - 1; i >= 0; i--) {
			if(array[i] === element) {
			   array.splice(i, 1);
			}
		}
	}
	
	this.addOutline = function (outlineObject) {
		
		var outline = {
			sceneMask: new THREE.Scene(),
			sceneOutline: new THREE.Scene()
		}
		
		outline.sceneMask.add(outlineObject.outlineMask);
		outline.sceneOutline.add(outlineObject.outlineColor);
		
		//this.scene.add(outlineObject.outlineMask);
		//this.scene.add(outlineObject.outlineColor);
		
		this.outlines.push(outline);
		
		return outline;
	}
	this.removeOutline = function (outlineObject) {
		
		this.removeFromArray(this.outlines, outlineObject);
	}
	
	this.removeFromScene = function (object) {
		this.scene.remove(object);
	}
	
	this.setMouseDownAction = function (action) {
		this.onMouseDownAction = action;
	}
	
	this.setMouseUpAction = function (action) {
		this.onMouseUpAction = action;
	}

	this.setStepAction = function (action) {
		this.onStepAction = action;
	}

	this.isOffScreen = function () {
		
		//if (this.$containerElement == undefined) {console.log("WHAT?"); return false;}
		var top = referenceToMe.$containerElement.offset().top;
		var height = referenceToMe.$containerElement.outerHeight();
		var y = $(window).scrollTop();

		return (y+$(window).outerHeight() < top || y > top+height);
	}
	
	function animate3D() {
	
		id = requestAnimationFrame(animate3D.bind(this));
		render();
	}

	this.renderOutlines = function() {
		
		var gl = this.renderer.domElement.getContext('webgl') || this.renderer.domElement.getContext('experimental-webgl');
		
		for (var i = 0; i < this.outlines.length; i++)
		{
		
			//http://stackoverflow.com/questions/26134077/passing-color-to-fragment-shader-from-javascript
			
			// Make the entire stencil solid first.
			gl.clearStencil(1);
			gl.clear(gl.STENCIL_BUFFER_BIT);
			
			// Whenever the value of a pixel is 1 in the mask, make that pixel = 0 in the stencil.
			// 0xFF is the identifier I think.
			gl.stencilFunc(gl.ALWAYS, 0, 0xFF);
			gl.stencilOp(gl.REPLACE, gl.GL_REPLACE, gl.GL_REPLACE);
			
			// Disable color (u can also disable here the depth buffers)
			gl.colorMask(false, false, false, false);

			// Write to stencil
			gl.enable(gl.STENCIL_TEST);
			
			gl.stencilMask(0xFF);
			
			// Mask (Create a stencil that we render the next pass into)
			this.renderer.render(this.outlines[i].sceneMask, this.camera);
			
			// Wherever the stencil is = 1, show the model.
			gl.stencilFunc(gl.EQUAL, 1, 0xFF);
			gl.stencilOp(gl.KEEP, gl.KEEP, gl.KEEP);
			
			// Enable color
			gl.colorMask(true, true, true, true);
			
			// Clear depth buffer (seems important)
			//this.renderer.clearDepth();
			this.renderer.render(this.outlines[i].sceneOutline, this.camera);
			
			// Disable stencil test;
			gl.disable(gl.STENCIL_TEST);
		
			this.outlines[i].sceneMask.updateMatrixWorld();
			this.outlines[i].sceneOutline.updateMatrixWorld();
		}
		
		
		//this.renderer.clearDepth();
		//this.renderer.render(this.sceneMask, this.camera);
	}
	
	function render() {

		// Detect if the viewer is too far off screen and stop rendering it.
		//if (this.isOffScreen()) {return;}
	
		renderer.clear();
		renderer.render(scene, camera);
		//me.renderOutlines();
		
		var delta = clock.getDelta();
		var theta = clock.getElapsedTime();

		if (onStepAction) onStepAction();

		// MouseChange needs to be set back to 0 between frames (as there's no event to check if the mouse is stationary).
		// This is likely a temporary solution.
		//me.mouseChange.x = 0;
		//me.mouseChange.y = 0;
		
		scene.updateMatrixWorld();
		
		jQuery.each(billboards, function(i, e){
			
			var v = new THREE.Vector3(e.position.x-camera.getWorldDirection().x, e.position.y, e.position.z-camera.getWorldDirection().z);
			e.lookAt(v);
		});
		
		/*if (me.mixer) {
			me.mixer.update(delta);
		}*/
		
		if (d3orbit && d3orbit.getOControls().enabled) d3orbit.update();
	}

	this.onHotspotMouseEnter = function (hitbox) {
		hitbox.hovered = true;
	}
	this.onHotspotMouseExit = function (hitbox) {
		hitbox.hovered = false;
	}

	this.AnimateCamera = function (targetTransform, onCompleteAction) {

		TweenLite.to(camera.position, 1, {
			x: targetTransform.position.x,
			y: targetTransform.position.y,
			z: targetTransform.position.z,
			onComplete: onCompleteAction
		});
		
		var r = {
			x: targetTransform.rotation.x,
			y: targetTransform.rotation.y,
			z: targetTransform.rotation.z
		};
		
		// Find the quickest rotation to move to.
		while (r.x > Math.PI) r.x -= Math.PI; while (r.x < -Math.PI) r.x += Math.PI;
		while (r.y > Math.PI) r.y -= Math.PI; while (r.y < -Math.PI) r.y += Math.PI;
		while (r.z > Math.PI) r.z -= Math.PI; while (r.z < -Math.PI) r.z += Math.PI;
		
		TweenLite.to(camera.rotation, 1, {
			x: r.x,
			y: r.y,
			z: r.z
		});
	}
	
	this.scaleSmooth = function (object, scale, time) {
		
		TweenLite.to(object.scale, time, {
			x : scale,
			y : scale,
			z : scale
		});
	}
	
	this.rotateCamera = function (rot) {

		TweenLite.to(this.camera.rotation, 1, {
			x : convertToRad(rot.x),
			y : convertToRad(rot.y),
			z : convertToRad(rot.z)
		});
	}

	this.moveObject = function (object, pos) {

		TweenLite.to(object.position, 1, {
			x : pos.x,
			y : pos.y,
			z : pos.z
		});
	}

	function convertToRad(val) {
		return val * Math.PI / 180;
	}

	// Check if the raycast (mouse) hit the selected object.
	this.GetRaycastHit = function (object) {

		// Update the picking ray with the camera and mouse position
		raycaster.setFromCamera(mousePos, camera);

		// calculate objects intersecting the picking ray
		var intersect = raycaster.intersectObject(object, false);

		return intersect;
	}
	
	// Check if the raycast (mouse) hit any object in the scene (and return the nearest one).
	this.GetRaycastNearestHit = function (objectArray) {

		// Update the picking ray with the camera and mouse position
		raycaster.setFromCamera(mousePos, camera);
		
		// Calculate objects intersecting the picking ray
		var intersects = raycaster.intersectObjects(objectArray, true);
		
		if (intersects.length == 0) return null; // No intersected objects.
		
		var distance = Infinity;
		var intersectNearest = null;
		for ( var i = 0; i < intersects.length; i++ ) {

			if (intersectNearest == null || intersects[i].distance < distance)
			{
				distance = intersects[i].distance;
				intersectNearest = intersects[i];
			}
		}
		
		return intersectNearest;
	}

	this.loadAnimatedModel = function (path) {

		var me = this;
	
		this.jsonLoader.load(path, function (geometry, materials) {

			materials.forEach(function (material) {
				material.skinning = true;
			});

			var mesh = new THREE.SkinnedMesh(geometry, new THREE.MeshFaceMaterial(materials));
			me.addToScene(mesh);

			// Actions
			mixer = new THREE.AnimationMixer(mesh);
			me.readAnimatedActions(mesh, geometry.animations);
		});
	}

	this.readAnimatedActions = function (mesh, anim) {
		// Record actions.
		mesh.actions = new Array();

		for (var i = 0; i < anim.length; i++) {
			var nameSplit = anim[i].name.split("_");

			// If this isn't a "swim" animation, don't read it (Blender sometimes exports extra, generated animations).
			//if (nameSplit[0] != "swim")	continue;

			//var group = nameSplit[1];

			// Create a new array to store animation groups (example: all animations dictating arm movements) in the same variable.
			//if (action[group] == undefined) action[group] = new Array();

			// And then, add the animation to the array.
			var a = mixer.clipAction(anim[i]);
			a.setEffectiveWeight(1).stop();
			mesh.actions.push(a);

			// And also log it for debug.
			console.log("Added " + anim[i].name);

			// Temporary: populate a list of action controllers.
			//$("#temp-list").append("<li id='"+i+"-temp-list'>"+anim[i].name+"</li>");

			setTimeout(function(){this.fadeAction(mesh, anim[i].name, i);}, 500);

			//initActionClick($("#"+i+"-temp-list"), group, action[group].length-1);
		}
	}
	
	// Allow for resizing of the 3D view in realtime!
	this.onWindowResize = function(){
	
		var me = referenceToMe;
	
		setTimeout(function(){
			
			var windowHeight = $("body").outerHeight();
			var headerHeight = $("#id-header").outerHeight();
			var footerHeight = $("footer").outerHeight();
			
			//console.log("Header: " + headerHeight);
			//console.log("Footer: " + footerHeight);
			
			// Resize the container element (needed a JavaScript solution).
			//me.$containerElement.css({"height": windowHeight-footerHeight-headerHeight});
									  
			//console.log("new height: " + me.$containerElement.outerHeight());
			
			me.camera.aspect = me.$containerElement.outerWidth() / me.$containerElement.outerHeight();
			me.camera.updateProjectionMatrix();

			me.renderer.setSize( me.$containerElement.outerWidth(), me.$containerElement.outerHeight() );
		},100);

	}

	this.remove = function () {
		
		cancelAnimationFrame(id);
		this.renderer.domElement.addEventListener('dblclick', null, false); //remove listener to render
		//this.scene = null;
		//this.projector = null;
		this.camera = null;
		//this.controls = null;
		this.$containerElement.empty();
		this.raycaster = null;
		
		window.removeEventListener( 'resize', referenceToResizeFunction, false);
	}
	
	this.fadeAction = function () {

		var activeActionName = 'swim1';

		return function (mesh, name, index) {

			if (activeActionName === name) {
				return;
			}

			// Fade out all actions that aren't this one.
			for (var i = 0; i < mesh.actions.length; i++) {
				if (i == index)
					continue;
				
				mesh.actions[i].fadeOut(.3);
			}

			//var f = action[group][index].play();
			var t = mesh.actions[index].play();
			t.setEffectiveWeight(1);

			//f.enabled = true;
			t.enabled = true;

			//f.crossFadeTo( t, .3 );
			//f.fadeOut(.3);
			t.fadeIn(.3);

			activeActionName = name;
		}

	}
	();
}