/**********************************************************************************************/
/* FPS view with click+drag controls (modified version of Threejs's equirectangular panorama) */
/**********************************************************************************************/
/* Author: Threejs, Milton Plotkin
/* Date: February 2th 2017 */

function D3OrbitControls(enableZoom) {
	
	this.$container;
	this.camera;
	var oControls;
	
	this.init = function (container, camera) {
	
		this.$container = container;
		this.camera = camera;
		var me = this;
		
		// Initiate OrbitControls plugin.
		oControls = new THREE.OrbitControls(camera, container[0]);

		oControls.enableDamping = true;
		oControls.dampingFactor = 0.1;
		oControls.rotateSpeed = 0.1;
		oControls.enableZoom = true;
		oControls.zoomSpeed = 1.0;
		oControls.minDistance = 85;
		oControls.maxDistance = 170;
		
		oControls.noPan = true; // Rotation only
		
		// Restrict vertical scrolling.
		oControls.minPolarAngle = Math.PI/6; // View Top
		oControls.maxPolarAngle = Math.PI/2; // View Bottom
		
		// Don't allow zooming.
		oControls.enableZoom = enableZoom;
		
		console.log("Orbit controls initiated");
	}
	
	this.getOControls = function() {
		return oControls;
	}
	
	this.update = function() {
		if (oControls) oControls.update();
	}
}