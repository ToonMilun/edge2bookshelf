@echo off
setlocal enabledelayedexpansion
set path="C:\Program Files\7-Zip";%path% 		REM Include 7z.

REM Remove temporary directories here (just in case).
if exist "___temp" rmdir /s /q "___temp" >nul
if exist "___help" rmdir /s /q "___help" >nul

REM Get current date (used in the copied .zips filename suffix).
set _PATCH=_PATCH
set zipSuffix=
For /f "tokens=2-4 delims=/ " %%a in ('date /t') do (set zipSuffix=%%a-%%b-%%c)
set zipSuffix=!_PATCH!(!zipSuffix!)

echo [101;93mWARNING^^![0m[93m This is a batch script, and will process MANY files once it's started. Please MAKE SURE you've read the README.txt and understand what this will do. If you are unsure, please close this program IMMEDIATELY^^![0m
echo [93m- Milton Plotkin[0m
echo.
pause
echo.

echo [101;93mSECONDARY WARNING^^![0m[93m This batch script may not properly process .zip files made before 2017. If something is wrong with the patched .zip, please re-zip it to make it compatible (extract it, then zip it again), then re-run the patch tool. As of this patch though, this problem shouldn't occur anymore.[0m
echo.
pause
echo.

REM Offer the option to auto unzip patched zips (for testing purposes).
echo [93mDo you want this script to automatically extract all .zips it creates (makes it easier to test the outcome of the patch)? [Y,N][0m
set _AUTOUNZIP=0
CHOICE /N
if !errorlevel!==1 (
	set _AUTOUNZIP=1
)
echo.

REM echo _AUTOUNZIP is !_AUTOUNZIP!


REM ----------------------
REM "help" DIRECTORY PATCH
REM ----------------------

echo [91mCopying and creating a patched version of "help" directory for later use.[0m

REM --------------
REM HELP
REM --------------

REM Patch the local "help" directory to the current version (will be copied into appropriate .zips later).
xcopy /s /y /v /i "help" "___help" >nul

REM Copy the js into the directory
copy /y /v "dd-viewport.js" "___help/mobile/dd-viewport.js"

REM Copy the css into the directory
copy /y /v "dd-player.css" "___help/mobile/dd-player.css"

REM Copy the logo image into the directory.
copy /y /v "logo.png" "___help/mobile/logo.png"

cd /D "___help"

setlocal
set _1="	^<link href="mobile/dd-player.css" rel="stylesheet" type="text/css" /^>"
set _2="		globals.strScale = "noscale";   // noscale ^| show all"
set _3="^<script src="mobile/dd-viewport.js" type="text/javascript" charset="utf-8"^>^</script^>"
set _4="g_bMinFlash = false; // Custom override. Never load the Flash version of the content."

REM Last parameter: 0 = append the line after. 1 = replace the line.
CALL :modifyHTML "index_lms_html5.html",15,%%_1%%,0
CALL :modifyHTML "index_lms_html5.html",113,%%_2%%,1
CALL :modifyHTML "story_html5.html",8,%%_1%%,0
CALL :modifyHTML "story_html5.html",83,%%_2%%,1

REM Find the line containing </html> in the *_html5.html files and insert the dd-viewport.js script just above it.
REM "delims=:" explained.
REM findstr will output #:text with the current settings used. The delims will split the output on :, making %%h = # (only the line number).
for /f "delims=:" %%h in ('findstr /i /n /b "</html>" "index_lms_html5.html"') do (
	set h1= %%h
	set /a h1-=1
	
	CALL :modifyHTML "index_lms_html5.html",!h1!,%%_3%%,0
)
for /f "delims=:" %%h in ('findstr /i /n /b "</html>" "story_html5.html"') do (
	set h1= %%h
	set /a h1-=1
	
	CALL :modifyHTML "story_html5.html",!h1!,%%_3%%,0
)
set brk=0
for /f "delims=:" %%h in ('findstr /i /n "g_bUseHtml5" "story.html"') do (
	if "!brk!"=="0" (
	
		set h1= %%h
		set /a h1-=1
		
		CALL :modifyHTML "story.html",!h1!,%%_4%%,0
		REM Only find the first instance of any line containing "g_bUseHtml5" anywhere.
		REM Cant use goto: inside of loop without breaking the script, so use this instead.
		set brk=1
		
		echo Firefox Flash content prevention fix applied to story.html
	)
)
endlocal

REM --------------
REM TABLET
REM --------------

REM Modify the two files that tablets will use.
setlocal
set _1="				^<a id="launch" href="#" class="button" target="_top"^>Launch^</a^>"
set _2="				var strLocation = location.href.replace^("index_lms", "story"^)^;"

REM CALL :modifyHTML "ioslaunch.html",110,%%_1%%,1
CALL :modifyHTML "index_lms.html",10,%%_2%%,1
endlocal

echo [92mDone.[0m

cd /D ".."

REM --------------
REM MAIN ZIP PATCH
REM --------------

REM For each directory in the root
for /d %%D in (*) do (
	
	REM For each .zip in the root.
	for /f "delims=" %%a in ('dir "%%D\*.zip" /b /a-d') do (
		echo ZIP found:          [95m%%a[0m
		
		REM Skip .zips which contain "_PATCH".
		echo %%a|findstr /i /l "!_PATCH!">nul
		if errorlevel 1 (
		
			echo --------------------------------------------------------------------------------
		
			set cloneZipName=%%~fD\%%~na!zipSuffix!.zip
		
			REM Valid .zip
			REM Clone the .zip (~n means "get only the name without the file extension").
			REM /v verify, /y force overwrite.
			copy /y /v "%%~fD\%%a" "!cloneZipName!"
			
			echo Cloned [95m%%a[0m to [91m%%~na!zipSuffix!.zip[0m
			
			REM Create a fake directory to store the file structure that will be merged with the .zip contents.
			mkdir "___temp"
			
			REM Extract the imsmanifest.xml.
			7z x "!cloneZipName!" "-o___temp" "imsmanifest.xml" -r -aoa >nul
			
			REM Extract the two .html files for each Section that will need to be modified to the ___temp folder.
			REM They will automatically create the "SectionX" directory structure that the .zip uses.
			7z x "!cloneZipName!" "-o___temp" "*_html5.html" -r -aoa >nul
			
			REM Also, extract the story.html file (it needs to be modified to fix Firefox automatically trying to load Flash instead of HTML5).
			7z x "!cloneZipName!" "-o___temp" "*story.html" -r -aoa >nul
			
			REM Also, extract ioslaunch.html and index_lms.html (they need to be updated in order to run better/at all on tablet devices).
			7z x "!cloneZipName!" "-o___temp" "*ioslaunch.html" -r -aoa >nul
			7z x "!cloneZipName!" "-o___temp" "*index_lms.html" -r -aoa >nul
			
			REM Check if there's a "help" file in the root (no -r, as we only want the root).
			REM Important to do this after the .html's have been extracted (to prevent them replacing the copied over "___help" file's .htmls).
			7z x "!cloneZipName!" "-o___temp" "help" -aoa >nul
			set globalHelp=
			if exist ___temp/help (
				set globalHelp=1
				echo [92mGlobal "help" directory found.[0m
				
				REM Copy in the patched "___help" directory to replace the one extracted from the .zip
				rmdir /s /q "___temp/help"
				xcopy /s /y /v /i "___help" "___temp/help" >nul
				
				echo [93mGlobal "help" directory replaced with patched version.[0m
				
			) else (
				set globalHelp=0
				echo [93mGlobal "help" directory NOT found. Assumed each Section has a local "help" directory.[0m
			)
			
			REM Get the list of Section* directory names inside this archive (each will need to have files extracted, modified, and re-added).
			REM Double escaping ! is necessary (due to enabledelayedexpansion).
			
			REM Important bugfix step: some Section* folders may be missing their modification date. If this is the case, then the 7z l(ist) won't list them. To solve this, all files which may contain Section* are listed, and the amount of sections used is = to the highest value found.
			set numSections=0
			
			FOR /F "tokens=*" %%S IN ('7z l "!cloneZipName!" "Section*"') DO (
				REM Split lines on spaces and on \.
				for /F "tokens=1-6 delims=\ " %%A in ("%%S") do (
					
					REM Get only the part of the file path that is "Section*"
					if not [%%F] == [] (
					
						set result=%%F
					
						if /i "!result:~0,7!"=="Section" (
							REM Get just the Section number, and assign it to numSections if its larger.
							set testNum=!result:~7,99!
							
							if "!testNum!" gtr "!numSections!" (
								set numSections=!testNum!
							)
						)
					)
				)
			)
			echo.
			echo [92m!numSections! Sections found in this unit.[0m
			echo.
			
			REM For each Section number.
			FOR /L %%G IN (1,1,!numSections!) DO (
			
				set section=Section%%G
			
				if !section!==!section:Section=! (
					REM Not a Section* directory
				) ELSE (
				
					REM !section! equals Section*.
					REM ...this turned out to be redundant. Extracting the *_html5.html files from the .zip creates these directories (but also the "help" directory).
				
					echo.
					echo ---------------------
					echo.
					
					echo Processing: !section!
					
					REM Create the directory for this Section.
					mkdir "___temp/!section!"
					
					REM And the mobile directory
					mkdir "___temp/!section!/mobile"
					
					REM Copy the css into the directory
					copy /y /v "dd-player.css" "___temp/!section!/mobile/dd-player.css"
					
					REM Copy the custom .js into the directory
					copy /y /v "dd-viewport.js" "___temp/!section!/mobile/dd-viewport.js"
					
					REM Copy the logo image into the directory.
					copy /y /v "logo.png" "___temp/!section!/mobile/logo.png"
					
					REM Copy the patched ___help directory into the SectionX directory if globalHelp isn't being used.
					if !globalHelp! LSS 1 (

						REM Remove the old (unpatched) help directory if it's there first.
						if exist "___temp/!section!/help" rmdir /s /q >nul
						
						REM Copy over the patched help directory into this "Section*" directory.
						mkdir "___temp/!section!/help"
						xcopy /s /y /v "___help" "___temp/!section!/help" >nul
						echo [93mCopied "help" directory into !section![0m
					)
					
					
					echo [91mModifying .html files...[0m
					
					
					REM Modify the first HTML (index_lms_html5.html)
					cd /D "___temp/!section!"
					
					REM for /f "delims=" %%a in ('findstr %findtext% %findfile%') do echo The full line of the string is %%a
					
					REM https://stackoverflow.com/questions/12655558/strange-behavior-with-special-characters-in-arguments-of-batch-functions
					setlocal
					set _1="	^<link href="mobile/dd-player.css" rel="stylesheet" type="text/css" /^>"
					set _2="		globals.strScale = "noscale";   // noscale ^| show all"
					set _3="^<script src="mobile/dd-viewport.js" type="text/javascript" charset="utf-8"^>^</script^>"
					set _4="g_bMinFlash = false; // Custom override. Never load the Flash version of the content."
					
					REM Last parameter: 0 = append the line after. 1 = replace the line.
					CALL :modifyHTML "index_lms_html5.html",7,%%_1%%,0
					CALL :modifyHTML "index_lms_html5.html",105,%%_2%%,1
					
					CALL :modifyHTML "story_html5.html",8,%%_1%%,0
					CALL :modifyHTML "story_html5.html",83,%%_2%%,1
					
					REM iPad fix for index_lms_html5
					set _i1="	^<script^>"
					set _i2="		var g_biOS = ^(navigator.userAgent.indexOf^("AppleWebKit/"^) ^> -1 ^&^& navigator.userAgent.indexOf^("Mobile/"^) ^> -1^)^;"
					set _i3="		if ^(g_biOS ^&^& true^)"
					set _i4="		{"
					set _i5="			var strLocation = location.href.replace^("index_lms_html5", "story"^)^;"
					set _i6="			location.replace^(strLocation^)^;"
					set _i7="		}"
					set _i8="	^</script^>"
					
					CALL :modifyHTML "index_lms_html5.html",4,%%_i1%%,0
					CALL :modifyHTML "index_lms_html5.html",5,%%_i2%%,0
					CALL :modifyHTML "index_lms_html5.html",6,%%_i3%%,0
					CALL :modifyHTML "index_lms_html5.html",7,%%_i4%%,0
					CALL :modifyHTML "index_lms_html5.html",8,%%_i5%%,0
					CALL :modifyHTML "index_lms_html5.html",9,%%_i6%%,0
					CALL :modifyHTML "index_lms_html5.html",10,%%_i7%%,0
					CALL :modifyHTML "index_lms_html5.html",11,%%_i8%%,0
					
					REM Find the line containing </html> in the *_html5.html files and insert the dd-viewport.js script just above it.
					REM "delims=:" explained.
					REM findstr will output #:text with the current settings used. The delims will split the output on :, making %%h = # (only the line number).
					for /f "delims=:" %%h in ('findstr /i /n /b "</html>" "index_lms_html5.html"') do (
						set h1= %%h
						set /a h1-=1
						
						CALL :modifyHTML "index_lms_html5.html",!h1!,%%_3%%,0
					)
					for /f "delims=:" %%h in ('findstr /i /n /b "</html>" "story_html5.html"') do (
						set h1= %%h
						set /a h1-=1
						
						CALL :modifyHTML "story_html5.html",!h1!,%%_3%%,0
					)
					REM Cant reuse the name "brk" (since it was used earlier. Caused bugs).
					set brk2=0
					for /f "delims=:" %%h in ('findstr /i /n "g_bUseHtml5" "story.html"') do (
						if "!brk2!"=="0" (
						
							set h1= %%h
							set /a h1-=1
							
							CALL :modifyHTML "story.html",!h1!,%%_4%%,0
							REM Only find the first instance of any line containing "g_bUseHtml5" anywhere.
							REM Cant use goto: inside of loop without breaking the script, so use this instead.
							set brk2=1
						
							echo Firefox Flash content prevention fix applied to story.html
						)
					)
					endlocal
					
					REM Modify the two files that tablets (iPad) will use.
					setlocal
					set _1="				^<a id="launch" href="#" class="button" target="_top"^>Launch^</a^>"
					set _2="				var strLocation = location.href.replace^("index_lms", "story"^)^;"
					
					CALL :modifyHTML "ioslaunch.html",110,%%_1%%,1
					CALL :modifyHTML "index_lms.html",10,%%_2%%,1
					endlocal
					
					echo [92mDone[0m
					
					cd /D "../.."
				)
			)
			
			REM Navigate inside of ___temp (so that "___temp" isn't the directory which gets added to the .zip).
			cd /D "___temp"
			
			REM IMSMANIFEST
			REM Modify the imsmanifest.xml to make it load the index_lms_html5.html file instead of the index_lms.html file.
			echo.
			echo [91mModifying imsmanifest.xml file...[0m
			setlocal
			REM Unintentionally replaces another instance of "index_lms.html" that isn't necessary to replace, but doesn't seem to have any adverse consequences when it does.
			CALL :findReplace "imsmanifest.xml","index_lms.html","index_lms_html5.html"
			endlocal
			
			REM Merge the directories and the imsmanifest into the archive.
			7z a "!cloneZipName!" "Section*"
			7z a "!cloneZipName!" "imsmanifest*"
			7z a "!cloneZipName!" "help"
			
			REM Delete the ___temp directory when you're done.
			
			echo.
			echo [93mRemoving ___temp directory.[0m
			echo.
			
			cd /D ".."
			
			rmdir /s /q "___temp"
			
			REM Extract the new .zip if that setting has been chosen.
			if !_AUTOUNZIP! GTR 0 (
				echo [93mUnzipping cloned .zip ^(this may take a while^)[0m
				mkdir %%~fD\%%~na!zipSuffix!
				7z x "!cloneZipName!" "-o%%~fD\%%~na!zipSuffix!" -r -aoa >nul
			)
			
			echo --------------------------------------------------------------------------------
			
		) else (
			REM Does contain "_PATCH".
		)
	)
)

echo.
echo [93mRemoving ___help directory.[0m
echo.

rmdir /s /q "___help"

echo.[30;102mDONE^^![0m

pause

:findReplace

setlocal enableextensions disabledelayedexpansion

set "search=%~2"
set "replace=%~3"

set "textFile=%~1"

for /f "delims=" %%i in ('type "%textFile%" ^& break ^> "%textFile%" ') do (

	set "line=%%i"
	setlocal enabledelayedexpansion
	>>"%textFile%" echo(!line:%search%=%replace%!
	endlocal
)

endlocal

exit /b

:modifyHTML
set tempfile=%random%-%random%.tmp
set line=0

REM echo %~3
							
REM DisableDelayedExpansion needs to be used here to prevent the .html lines being parsed by Batch (example: <!DOCTYPE html> becoming <DOCTYPE html>)
setlocal DisableDelayedExpansion enableextensions

REM The parameters are to force blank lines to also be read.
for /f "tokens=1* delims=]" %%i in ('type "%~1" ^| find /V /N ""') do (
	
	REM Done so that if %%i == "", then "ECHO is off" won't be printed.
	REM Quotes around assignment will auto-escape any special characters in the line.
	set "str=%%j"
	set /a "line+=1"
	
	setlocal enabledelayedexpansion
	
	if !line!==%~2 (
		if %~4 LSS 1 (
			echo.!str!>>!tempfile!
		)
		echo.%~3>>!tempfile!
	) else (
	
		echo.!str!>>!tempfile!
		REM ..
	)
	
	endlocal
)
endlocal

REM Replace the .html file with the temp file.
del %~1
ren !tempfile! %~1

EXIT /B 0