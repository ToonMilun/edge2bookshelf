@echo off
setlocal enableextensions enabledelayedexpansion
set path="C:\Program Files\7-Zip";%path% 		REM Include 7z.

CD /D %~dp0

REM Intro
REM -------------------------------------------------------------
echo [107;30m - Edge2Bookshelf - [0m
echo.

echo [101;93mWARNING^^![0m[93m This is a batch script, and will process MANY files once it's started. Please MAKE SURE you've read the README.txt and understand what this will do. If you are unsure, please close this program IMMEDIATELY^^![0m
echo [93m- Milton Plotkin[0m
echo.
echo.

pause

echo.

REM Debug
REM -------------------------------------------------------------
REM If true, will not remove or skip extracting to __tmp 
set _DEBUGMODE=0

REM Cleanup
REM -------------------------------------------------------------
REM Remove temporary directories here (just in case).
if !_DEBUGMODE!==0 (
	if exist "___tmp" rmdir /s /q "___tmp" >nul
	REM Remove the previously generated EPUB.epub file
	if exist "SCORM\EPUB.epub" (

		echo [91mA generated EPUB.epub already exists in the SCORM folder.[0m
		set /p areyousure=[91mIf you proceed it WILL be removed. Are you sure? [0m
		if /I "!areyousure!" NEQ "Y" (
			echo.
			echo [91mPlease move/rename the EPUB.epub in the SCORM folder, then run this batch script again.[0m
			echo.
			pause
			goto :eof
		) else (
			echo.
		)
		del /S "SCORM\EPUB.epub"
	)
)

REM Date and time
REM -------------------------------------------------------------
REM Store the current date and time.
for /f "tokens=2 delims==" %%G in ('wmic os get localdatetime /value') do set datetime=%%G

set year=!datetime:~0,4!

set month=!datetime:~4,2!

set day=!datetime:~6,2!

set hour=!time:~0,2!
if "!hour:~0,1!" == " " set hour=0!hour:~1,1!

set minute=!time:~3,2!
if "!minute:~0,1!" == " " set minute=0!minute:~1,1!

set second=!time:~6,2!
if "!second:~0,1!" == " " set second=0!second:~1,1!


REM Filename
REM -------------------------------------------------------------
REM Get current date (used in the copied .zips filename suffix).
set _EPUB=_EPUB
set zipSuffix=
For /f "tokens=2-4 delims=/ " %%a in ('date /t') do (set zipSuffix=%%a-%%b-%%c)
set zipSuffix=!_EPUB!(!zipSuffix!)


REM Main
REM -------------------------------------------------------------

REM Get ISBN
REM -------------------------------------------------------------
set /P isbn="What is the ISBN (do NOT enter dashes)? It will start with '978': "


REM Confirm zip order
REM -------------------------------------------------------------
REM For each directory in the SCORM folder
for /d %%D in (SCORM) do (

	REM For each .zip in the root of this (SCORM) folder.
	REM Confirm with the user that this is what they want.
	echo [93mZips found:[0m
	for /f "delims=" %%a in ('dir "%%D\*.zip" /b /a-d') do (
		echo [96m%%a[0m
	)
	echo.

	set /p areyousure=[93mAre these zips in the correct order? [0m
	if /I "!areyousure!" NEQ "Y" (
		echo.
		echo [91mPlease rename the .zips to get them in the correct order, then run this batch script again.[0m
		echo.
		pause
		goto :eof
	) else (
		echo.
	)
)


REM Create temp directory
REM ------------------------------------------------------
REM Create the temporary directory.
mkdir "___tmp"
echo [93mCopying epub_files...[0m
if !_DEBUGMODE!==0 (
	xcopy "epub_files" "___tmp" /s /e /q>nul
)
echo [93mDone.[0m
echo.


REM Extract and clean up zips
REM -------------------------------------------------------------
REM For each directory in the SCORM folder
for /d %%D in (SCORM) do (
	
	REM For each .zip in the root of this (SCORM) folder.
	set /a chapterNum=1
	set /a chapterCount=0
	for /f "delims=" %%a in ('dir "%%D\*.zip" /b /a-d') do (

		cd /d %~dp0

		REM Extract the .zip to a corresponding chapter number folder.
		echo [93mExtracting zip to chapter!chapterNum!: [96m%%a[0m
		if !_DEBUGMODE!==0 (
			set zipName=%%~fD\%%~na.zip
			mkdir "___tmp/EPUB/content/chapter!chapterNum!"

			REM Extract everything from inside the .zip
			7z x "!zipName!" "-o___tmp/EPUB/content/chapter!chapterNum!" "*" -r -aoa >nul
		)

		REM Navigate inside of that chapter.
		cd /d "___tmp/EPUB/content/chapter!chapterNum!"

		REM Remove empty folders and unneeded files
		REM ------------------------------------------------------
		echo [93mRemoving Unneeded files and empty directories.[0m
		REM Remove Thumbs.db (Windows 10 generated files).
		del /S Thumbs.db 2> nul
		REM Remove empty directories (loops through all directories; rd won't remove a directory if it has something in it when used with these settings).
		for /f "delims=" %%# in ('dir /s /b /ad ^| sort /r') do (
			REM The two nul's prevent the command printing "cannot remove empty directory" (that message gets printed as an error).
			rmdir /q "%%#"> nul 2> nul
		)

		echo [93mDone[0m
		echo.

		set /a chapterNum+=1
		set /a chapterCount+=1

		REM VERY IMPORTANT to include this (loops breaks if otherwise).
		cd /d %~dp0
	)
)


REM Create package.opf
REM ------------------------------------------------------
REM Generate the package.opf file, along with a list of ALL files contained in the .epub (minus package.opf).

REM Navigate inside of ___tmp/EPUB.
cd /d %~dp0
cd /d "___tmp/EPUB"

REM Begin write to file
SET filename=package.opf

echo [93m--------------------------------------------------------------------------------[0m
echo.
echo [93mGenerating package.opf. This will take a while.[0m
echo.

echo [93mDate modified:[0m
set modifieddate=!year!-!month!-!day!T!hour!:!minute!:!second!Z
echo [96m!modifieddate![0m
echo.


REM Get unit and topic names
REM --------------------------------------------------------
for /f "tokens=* delims=" %%# in ('..\..\lib\jsonextractor\jsonextractor.bat "content\chapter1\epubData.json" "unitTitle"') do (
	set "unitname=%%~#"
)

ECHO [93mUnit name: [96m!unitname![0m
echo.
REM ECHO "[93mTopic name: [96m!topicname![0m"
REM echo.

REM This first line will clear the file (by using > instead of >>).
ECHO ^<?xml version='1.0' encoding='utf-8'?^> > !filename!
ECHO ^<package xmlns="http://www.idpf.org/2007/opf" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:dcterms="http://purl.org/dc/terms/" version="3.0" xml:lang="en" unique-identifier="pub-identifier"^> >> !filename!
ECHO 	^<metadata^> >> !filename!
ECHO 		^<dc:identifier id="pub-identifier"^>urn:isbn:!isbn!^</dc:identifier^> >> !filename!
ECHO 		^<dc:title id="pub-title"^>!unitname!^</dc:title^> >> !filename!
ECHO 		^<dc:language id="pub-language"^>en^</dc:language^> >> !filename!
ECHO 		^<dc:date^>!modifieddate!^</dc:date^> >> !filename!
ECHO 		^<meta property="dcterms:modified"^>!modifieddate!^</meta^> >> !filename!

REM Authors
REM ------------------------------------------------------
REM Prompt the user for a list of authors (some of our old Adapt content didn't have them listed).

echo [93mAuthors:[0m

for /f "tokens=* delims=" %%# in ('..\..\lib\jsonextractor\jsonextractor.bat "content\chapter1\epubData.json" "authorsCount"') do (
	set "authorsCount=%%~#"
)
set /a authorsCount-=1
FOR /L %%i IN (0,1,!authorsCount!) DO (
  for /f "tokens=* delims=" %%# in ('..\..\lib\jsonextractor\jsonextractor.bat "content\chapter1\epubData.json" "authors[%%i].name"') do (
	set "authorname=%%~#"
	ECHO 		^<dc:creator id="pub-creator_%%i"^>!authorname!^</dc:creator^> >> !filename!
	ECHO [96m%%i: !authorname![0m
  )
)
echo [0m


REM		ECHO 		^<dc:creator id="pub-creator12"^>Didasko Digital^</dc:creator^> >> !filename!
REM		ECHO 		^<dc:contributor^>TEMP_AUTHOR^</dc:contributor^> >> !filename!
ECHO 		^<dc:publisher^>VitalSource^</dc:publisher^> >> !filename!
ECHO 		^<dc:rights^>Copyright © !year! Didasko Digital^</dc:rights^> >> !filename!
ECHO 		^<dc:language^>en^</dc:language^> >> !filename!
ECHO 	^</metadata^> >> !filename!
ECHO 	^<manifest^> >> !filename!

REM ------------------------------------------------------------------
REM PACKAGE ITEMS
REM ------------------------------------------------------------------
REM Add references to all files across ALL chapters to package.opf
REM ------------------------------------------------------------------
REM "delims=" adds support for directories with spaces.

set fileCount=0
for /F "delims=" %%A in ('dir /s/b /a:-d "*.*"') do set /a fileCount+=1
echo [93mFile count = !fileCount![0m
echo.
set index=0

REM Add the toc.xhtml and the cover image files (hardcoded)
REM if !_DEBUGMODE!==0 (
call :writePackageItem !filename!,htmltoc,toc.xhtml,application/xhtml+xml,nav
call :writePackageItem !filename!,coverimage,covers/cover_lrg.jpg,image/jpeg,cover-image
REM Cover page + cover page CSS.
call :writePackageItem !filename!,cover,content/cover.xhtml,application/xhtml+xml
call :writePackageItem !filename!,cover-css,content/cover.css,text/css
REM )

REM For each chapter folder
cd /d %~dp0
cd /d "___tmp/EPUB"

set /a chapterNum=1

REM for loop used here to ensure counting goes 1,2,3,4... instead of 1,10,2,3,4...
FOR /L %%Z IN (1,1,!chapterCount!) DO (
		
	cd /d %~dp0
	cd /d "___tmp/EPUB/content/chapter%%Z"
	set filename=../../package.opf

	echo [93mchapter%%Z[0m
	echo [93m--------------------------------------------------------------------------------[0m


	REM For each item in this chapter.
	for /F "delims=" %%A in ('dir /s/b /a:-d "*.*"') do (
		set B=%%A
		REM File extension.
		set ext=%%~xA
		REM File id
		set /a index+=1
		REM File href (relative to location of package.opf)

		set href=!B:%CD%\=!
		set type=ERROR
		set done=0

		REM ECHO TODO prevent the opf itself from being included in its own items.
		REM ECHO TODO add the 3 unique items to the package.opf
		REM ECHO TODO populate the opf with data from the xml
		REM ECHO Resolve the remaining significant epub_check errors.

		REM CALL :wubba !filename!,!index!,!href!,"D"

		REM https://stackoverflow.com/questions/32460177/how-can-i-find-out-a-files-mime-typecontent-type-on-windows
		REM Skip Thumbs.db files.
		REM NOTE:
		REM file.exe can be quite slow, and returns incorrect MIME types for certain files.
		REM for such cases, the types have been hardcoded.
		REM DO NOT try to trick it (aka, do not rename *.png files to *.txt for no reason. Have the file extension reflect the file type).

		REM content/chapterX/index.html
		if "!href!"=="content\chapter%%Z\index.html" if !done!==0 (
			call :writePackageItem !filename!,index-!chapterNum!,"!href!",text/html,scripted
			REM call :writePackageItem !filename!,index-!chapterNum!,"!href!",application/xhtml+xml,scripted
			set /a index-=1
			set done=1
		)

		REM Image files
		if "!ext!"==".jpg" if !done!==0 (
			call :writePackageItem !filename!,!index!,"!href!",image/jpeg
			set done=1
		)
		if "!ext!"==".jpeg" if !done!==0 (
			call :writePackageItem !filename!,!index!,"!href!",image/jpeg
			set done=1
		)
		if "!ext!"==".png" if !done!==0 (
			call :writePackageItem !filename!,!index!,"!href!",image/png
			set done=1
		)
		if "!ext!"==".svg" if !done!==0 (
			call :writePackageItem !filename!,!index!,"!href!",image/svg+xml
			set done=1
		)
		if "!ext!"==".gif" if !done!==0 (
			call :writePackageItem !filename!,!index!,"!href!",image/gif
			set done=1
		)

		REM Scripts
		if "!ext!"==".js" if !done!==0 (
			call :writePackageItem !filename!,!index!,"!href!",text/javascript
			set done=1
		)
		if "!ext!"==".json" if !done!==0 (
			call :writePackageItem !filename!,!index!,"!href!",application/json
			set done=1
		)
		if "!ext!"==".html" if !done!==0 (
			call :writePackageItem !filename!,!index!,"!href!",text/html
			set done=1
		)
		REM Important to allow remote-resources in css files.
		if "!ext!"==".css" if !done!==0 (
			call :writePackageItem !filename!,!index!,"!href!",text/css,remote-resources
			set done=1
		)
		if "!ext!"==".less" if !done!==0 (
			call :writePackageItem !filename!,!index!,"!href!",text/plain
			set done=1
		)
		if "!ext!"==".xml" if !done!==0 (
			call :writePackageItem !filename!,!index!,"!href!",text/xml
			set done=1
		)
		if "!ext!"==".xhtml" if !done!==0 (
			call :writePackageItem !filename!,!index!,"!href!",application/xhtml+xml
			set done=1
		) 

		REM Use the (slower, but more accurate) file.exe if the file type is still unknown.
		REM And ignore all *.db files (Thumbs.db created by Windows 10).
		if NOT "!ext!"==".db" if !done!==0 (
			for /f "tokens=*" %%i in ('..\..\..\..\lib\file\bin\file --mime-type -b "!B!"') do (
				set type=%%i
				call :writePackageItem !filename!,!index!,"!href!",!type!,FILEEXE
			)
		)
	)

	set /a chapterNum+=1
	echo.
)

cd /d %~dp0
cd /d "___tmp/EPUB"
set filename=package.opf

ECHO 	^</manifest^> >> !filename!
ECHO 	^<spine^> >> !filename!

REM Add the cover to the spine
REM ----------------------------------------------------
ECHO 		^<itemref idref="id_cover"/^> >> !filename!

REM Create an entry for each chapters index.html file
REM ----------------------------------------------------
set /a chapterNum=1
for /d %%D in ("content/chapter*") do (
	ECHO 		^<itemref idref="id_index-!chapterNum!"/^> >> !filename!
	set /a chapterNum+=1
)

ECHO 	^</spine^> >> !filename!
ECHO ^</package^> >> !filename!


REM Generate the toc.xhtml file
REM ----------------------------------------------------

cd /d %~dp0
cd /d "___tmp/EPUB"

echo [93mCreating the toc.xhtml file...[0m
echo.

echo [93mTopics:[0m

set filename=toc.xhtml

ECHO ^<?xml version="1.0" encoding="UTF-8" standalone="no"?^> > !filename!
ECHO ^<^^!DOCTYPE html^> >> !filename!
ECHO ^<html xmlns="http://www.w3.org/1999/xhtml" xmlns:epub="http://www.idpf.org/2007/ops" xml:lang="en" lang="en"^> >> !filename!
ECHO	^<head^> >> !filename!
ECHO		^<meta http-equiv="default-style" content="text/html; charset=utf-8"/^> >> !filename!
ECHO		^<title^>!unitname!^</title^> >> !filename!
ECHO	^</head^> >> !filename!
ECHO	^<body^> >> !filename!
ECHO		^<nav epub:type="toc" id="toc" role="doc-toc"^> >> !filename!
ECHO			^<h1^>Table of Contents^</h1^> >> !filename!
ECHO			^<ol^> >> !filename!
REM ECHO				^<li^>^<a href="content/cover.xhtml"^>Cover^</a^></li> >> !filename!

REM Create an entry for each chapter's index.html file full of pagelinks to each of its blocks.
REM ----------------------------------------------------
set /a tocId=1
set /a chapterNum=1
for /d %%D in ("content/chapter*") do (
	
	REM Read the topicname from Adapt's JSON
	REM ------------------------------------
	for /f "tokens=* delims=" %%# in ('..\..\lib\jsonextractor\jsonextractor.bat "content\chapter!chapterNum!\epubData.json" "topicTitle"') do (
		set "topicname=%%~#"
	)
	echo [96m!chapterNum!: !topicname!:[0m
	ECHO				^<li id="id_!tocId!"^>^<a href="content/chapter!chapterNum!/index.html#pg1"^>!topicname!^</a^> >> !filename!
	ECHO					^<ol^> >> !filename!
	set /a tocId+=1

	REM Create a page entry for each block
	REM ----------------------------------
	for /f "tokens=* delims=" %%# in ('..\..\lib\jsonextractor\jsonextractor.bat "content\chapter!chapterNum!\epubData.json" "blocksCount"') do (
		set "blocksCount=%%~#"
	)
	set /a blocksCount-=1
	FOR /L %%i IN (0,1,!blocksCount!) DO (
		for /f "tokens=* delims=" %%# in ('..\..\lib\jsonextractor\jsonextractor.bat "content\chapter!chapterNum!\epubData.json" "blocks[%%i].title"') do (
			set "blockTitle=%%~#"
			set "j=%%i"
			set /a j+=1
			ECHO						^<li id="id_!tocId!"^>^<a href="content/chapter!chapterNum!/index.html#pg!j!"^>!blockTitle!^</a^>^</li^> >> !filename!
			ECHO [95m!j!: !blockTitle![0m
		)
		set /a tocId+=1
	)
	ECHO					^</ol^> >> !filename!
	ECHO				^</li^> >> !filename!
	
	set /a chapterNum+=1
)

ECHO			^</ol^> >> !filename!
ECHO		^</nav^> >> !filename!
ECHO		^<nav epub:type="page-list" hidden="hidden"^> >> !filename!
ECHO			^<ol^> >> !filename!

REM Cover
REM ----------------------------------------------------------
ECHO				^<li^>^<a href="content/cover.xhtml"^>0-0^</a^>^</li^> >> !filename!

REM Page list
REM ----------------------------------------------------------
echo.
echo [93mGenerating page-list elements. Please wait...[0m

set /a chapterNum=1
for /d %%D in ("content/chapter*") do (
	
	REM Get the blocks count for this chapter.
	for /f "tokens=* delims=" %%# in ('..\..\lib\jsonextractor\jsonextractor.bat "content\chapter!chapterNum!\epubData.json" "blocksCount"') do (
		set "blocksCount=%%~#"
	)
	set /a blocksCount-=1

	REM Add in each block's title as a page.
	FOR /L %%i IN (0,1,!blocksCount!) DO (
		set "j=%%i"
		set "k=%%i"
		set /a j+=1
		ECHO				^<li^>^<a href="content/chapter!chapterNum!/index.html#pg!j!"^>!chapterNum!-!k!^</a^>^</li^> >> !filename!
	)

	set /a chapterNum+=1
)

ECHO			^</ol^> >> !filename!
ECHO		^</nav^> >> !filename!
ECHO	^</body^> >> !filename!
ECHO ^</html^> >> !filename!

echo.
echo [93mDone.[0m
echo.

if !_DEBUGMODE!==0 (
	
	REM Create a Zip of the ___tmp folder.
	REM ----------------------------------------------------
	REM Do NOT include Thumbs.db files in the .zip.
	echo [93mCreating the .epub using the ___tmp folder...[0m
	cd /D ../
	REM 7z a -r testZip.zip * -x^^!Thumbs.db
	REM 7z a testZip.zip -x^^!Thumbs.db

	7z a -r EPUB.zip * m=Deflate -x^^!Thumbs.db
	REM Rename the .zip to .epub
	REM (important to do this AFTER it's been created as a .zip. Speeds up Bookshelf loading by ~100x).
	rename "EPUB.zip" "!isbn! - !unitname!.epub"

	REM Move the newly created EPUB into the SCORM folder with the other .zips.
	move "!isbn! - !unitname!.epub" "../SCORM"
)

echo [93mDone.[0m
echo.


pause

REM Remove the temporary directory.
cd /D ../
if !_DEBUGMODE!==0 (
	if exist "___tmp" rmdir /s /q "___tmp" >nul
)

EXIT /B 0
EndLocal
ECHO ON

REM FUNCTIONS
REM ----------------------------------------------------

:writePackageItem
	setlocal enableextensions enabledelayedexpansion

	set "filename=%~1"
	set "index=%~2"
	set "href=%~3"
	set "type=%~4"
	set "properties=%~5"

	REM Do a different colored ECHO if this was calculated with file.exe (purely for debug info as file.exe is slower).
	set fileexe=0
	
	REM properties parameter is optional
	if defined properties (
		if "!properties!"=="FILEEXE" (
			set properties=
			set fileexe=1
		) else (
			set properties=properties="!properties!"
		) 
	) else (
		set properties=
	)

	REM Replace all backslashes with frontslashes in the URL.
	set "href=!href:\=/!"

	if [!properties!]==[] (
		ECHO 		^<item id="id_!index!" href="!href!" media-type="!type!"/^> >> !filename!
	) else (
		ECHO 		^<item id="id_!index!" href="!href!" media-type="!type!" !properties!/^> >> !filename!
	)
	
	if !fileexe!==0 (
		ECHO !index! --- !href! --- !type!
	) else (
		ECHO [96m!index! --- !href! --- !type![0m
	)

	endlocal
	EXIT /B 0
REM ----------------------------------------------------