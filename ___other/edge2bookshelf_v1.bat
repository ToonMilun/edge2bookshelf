@echo off
setlocal enableextensions enabledelayedexpansion
set path="C:\Program Files\7-Zip";%path% 		REM Include 7z.

CD /D %~dp0

REM Intro
REM -------------------------------------------------------------
echo [107;30m - Edge2Bookshelf - [0m
echo.

echo [101;93mWARNING^^![0m[93m This is a batch script, and will process MANY files once it's started. Please MAKE SURE you've read the README.txt and understand what this will do. If you are unsure, please close this program IMMEDIATELY^^![0m
echo [93m- Milton Plotkin[0m
echo.
REM pause
echo.

echo [101;93mSECONDARY WARNING^^![0m[93m This batch script may not properly process .zip files made before 2017. If something is wrong with the patched .zip, please re-zip it to make it compatible (extract it, then zip it again), then re-run the patch tool. After applying this patch though, this problem shouldn't occur anymore.[0m
echo.
REM pause
echo.


echo [91mThis batch script is for Edge SCORMs.[0m
set /p areyousure=[91mDo the SCORMs in the SCORM folder match this description? [0m
echo.
if /I "!areyousure!" NEQ "Y" (
	echo [91mPlease use the appropriate patch tool if applicable.[0m
	echo.
	pause
	goto :eof
)


REM Debug
REM -------------------------------------------------------------
REM If true, will not remove or skip extracting to __tmp 
set _DEBUGMODE=0

REM Cleanup
REM -------------------------------------------------------------
REM Remove temporary directories here (just in case).
if !_DEBUGMODE!==0 (
	if exist "___tmp" rmdir /s /q "___tmp" >nul
	REM Remove the previously generated EPUB.epub file
	if exist "SCORM\EPUB.epub" (

		echo [91mA generated EPUB.epub already exists in the SCORM folder.[0m
		set /p areyousure=[91mIf you proceed it WILL be removed. Are you sure? [0m
		if /I "!areyousure!" NEQ "Y" (
			echo.
			echo [91mPlease move/rename the EPUB.epub in the SCORM folder, then run this batch script again.[0m
			echo.
			pause
			goto :eof
		) else (
			echo.
		)
		del /S "SCORM\EPUB.epub"
	)
)

REM Date and time
REM -------------------------------------------------------------
REM Store the current date and time.
for /f "tokens=2 delims==" %%G in ('wmic os get localdatetime /value') do set datetime=%%G

set year=!datetime:~0,4!

set month=!datetime:~4,2!

set day=!datetime:~6,2!

set hour=!time:~0,2!
if "!hour:~0,1!" == " " set hour=0!hour:~1,1!

set minute=!time:~3,2!
if "!minute:~0,1!" == " " set minute=0!minute:~1,1!

set second=!time:~6,2!
if "!second:~0,1!" == " " set second=0!second:~1,1!


REM Filename
REM -------------------------------------------------------------
REM Get current date (used in the copied .zips filename suffix).
set _EPUB=_EPUB
set zipSuffix=
For /f "tokens=2-4 delims=/ " %%a in ('date /t') do (set zipSuffix=%%a-%%b-%%c)
set zipSuffix=!_EPUB!(!zipSuffix!)


REM Main
REM -------------------------------------------------------------

REM Confirm zip order
REM -------------------------------------------------------------
REM For each directory in the SCORM folder
for /d %%D in (SCORM) do (

	REM For each .zip in the root of this (SCORM) folder.
	REM Confirm with the user that this is what they want.
	echo [93mZips found:[0m
	for /f "delims=" %%a in ('dir "%%D\*.zip" /b /a-d') do (
		echo [96m%%a[0m
	)
	echo.

	set /p areyousure=[93mAre these zips in the correct order? [0m
	if /I "!areyousure!" NEQ "Y" (
		echo.
		echo [91mPlease rename the .zips to get them in the correct order, then run this batch script again.[0m
		echo.
		pause
		goto :eof
	) else (
		echo.
	)
)


REM Create temp directory
REM ------------------------------------------------------
REM Create the temporary directory.
mkdir "___tmp"
echo [93mCopying epub_files...[0m
if !_DEBUGMODE!==0 (
	xcopy "epub_files" "___tmp" /s /e /q>nul
)
echo [93mDone.[0m
echo.


REM Extract and clean up zips
REM -------------------------------------------------------------
REM For each directory in the SCORM folder
for /d %%D in (SCORM) do (
	
	REM For each .zip in the root of this (SCORM) folder.
	set /a chapterNum=1
	for /f "delims=" %%a in ('dir "%%D\*.zip" /b /a-d') do (

		cd /d %~dp0

		REM Extract the .zip to a corresponding chapter number folder.
		echo [93mExtracting zip to chapter!chapterNum!: [96m%%a[0m
		if !_DEBUGMODE!==0 (
			set zipName=%%~fD\%%~na.zip
			mkdir "___tmp/EPUB/content/chapter!chapterNum!"
			7z x "!zipName!" "-o___tmp/EPUB/content/chapter!chapterNum!" "*/*" -r -aoa >nul
		)

		REM Navigate inside of that chapter.
		cd /d "___tmp/EPUB/content/chapter!chapterNum!"

		REM Remove empty folders and unneeded files
		REM ------------------------------------------------------
		echo [93mRemoving Unneeded files and empty directories.[0m
		REM Remove Thumbs.db (Windows 10 generated files).
		del /S Thumbs.db 2> nul
		REM Remove debug.log (Edge generated files).
		del /S debug.log 2> nul
		REM Remove *.sh and .less (LESSCSS related files which aren't used to display content).
		del /S _lessCompiler.sh 2> nul
		del /S _lessDidaskoStylesCompiler.sh 2> nul
		del /S *.less 2> nul
		REM Remove *.an (Adobe Edge editable files; not used for displaying content).
		REM del /S *.an On second thought better keep them there for safety.
		REM Remove empty directories (loops through all directories; rd won't remove a directory if it has something in it when used with these settings).
		for /f "delims=" %%# in ('dir /s /b /ad ^| sort /r') do (
			REM The two nul's prevent the command printing "cannot remove empty directory" (that message gets printed as an error).
			rmdir /q "%%#"> nul 2> nul
		)
		REM Remove the "placeholder" directory inside of the slides directory (it isn't used by Bookshelf)
		for /d %%G in ("section1/assets/slides/placeholder") do rmdir /s /q "%%~G" 2> nul

		echo [93mDone[0m
		echo.

		set /a chapterNum+=1

		REM VERY IMPORTANT to include this (loops breaks if otherwise).
		cd /d %~dp0
	)
)


REM Create package.opf
REM ------------------------------------------------------
REM Generate the package.opf file, along with a list of ALL files contained in the .epub (minus package.opf).

REM Navigate inside of ___tmp/EPUB.
cd /d %~dp0
cd /d "___tmp/EPUB"

REM Begin write to file
SET filename=package.opf

echo [93m--------------------------------------------------------------------------------[0m
echo.
echo [93mGenerating package.opf. This will take a while.[0m
echo.

echo [93mDate modified:[0m
set modifieddate=!year!-!month!-!day!T!hour!:!minute!:!second!Z
echo [96m!modifieddate![0m
echo.

REM Read XML data: Read values from the unitinfo.xml for the first chapter (all the .zips will have the same unitname in their unitinfo.xml).
for /f "tokens=* delims=" %%# in ('..\..\lib\xpath\xpath.bat "content/chapter1/section1/unitinfo.xml" "//unitname"') do set "unitname=%%#"
echo [93mUnit name:[0m
echo [96m!unitname![0m
echo.

REM This first line will clear the file (by using > instead of >>).
ECHO ^<?xml version='1.0' encoding='utf-8'?^> > !filename!
ECHO ^<package xmlns="http://www.idpf.org/2007/opf" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:dcterms="http://purl.org/dc/terms/" version="3.0" xml:lang="en" unique-identifier="pub-identifier"^> >> !filename!
ECHO 	^<metadata^> >> !filename!
ECHO 		^<dc:identifier id="pub-identifier"^>urn:isbn:9780000000000^</dc:identifier^> >> !filename!
ECHO 		^<dc:title id="pub-title"^>!unitname!^</dc:title^> >> !filename!
ECHO 		^<dc:language id="pub-language"^>en^</dc:language^> >> !filename!
ECHO 		^<dc:date^>TEMP_PUBLICATION_DATE^</dc:date^> >> !filename!
ECHO 		^<meta property="dcterms:modified"^>!modifieddate!^</meta^> >> !filename!

REM Read value from authors.xml (get a list of creators).
REM Read from chapter1 (all authors.xmls should be the same across the .zips).
echo [93mAuthors:[0m
set index=0
for /f "tokens=* delims=" %%# in ('..\..\lib\xpath\xpath.bat "content/chapter1/section1/authors.xml" "//name"') do (
	REM TODO: Add the author's role too
	REM https://www.w3.org/publishing/epub3/epub-packages.html#sec-opf-dccontributor
	ECHO 		^<dc:creator id="pub-creator_!index!"^>%%#^</dc:creator^> >> !filename!
	echo [96m!index!: %%#[0m
	set /a index+=1
)
echo.

REM		ECHO 		^<dc:creator id="pub-creator12"^>Didasko Digital^</dc:creator^> >> !filename!
REM		ECHO 		^<dc:contributor^>TEMP_AUTHOR^</dc:contributor^> >> !filename!
ECHO 		^<dc:publisher^>VitalSource^</dc:publisher^> >> !filename!
ECHO 		^<dc:rights^>Copyright © !year! Didasko Digital^</dc:rights^> >> !filename!
ECHO 		^<dc:language^>en^</dc:language^> >> !filename!
ECHO 	^</metadata^> >> !filename!
ECHO 	^<manifest^> >> !filename!

REM ------------------------------------------------------------------
REM PACKAGE ITEMS
REM ------------------------------------------------------------------
REM Add references to all files across ALL chapters to package.opf
REM ------------------------------------------------------------------
REM "delims=" adds support for directories with spaces.

set fileCount=0
for /F "delims=" %%A in ('dir /s/b /a:-d "*.*"') do set /a fileCount+=1
echo [93mFile count = !fileCount![0m
echo.
set index=0

REM Add the toc.xhtml and the cover image files (hardcoded)
if !_DEBUGMODE!==0 (
	call :writePackageItem !filename!,htmltoc,toc.xhtml,application/xhtml+xml,nav
	call :writePackageItem !filename!,coverimage,covers/cover_lrg.jpg,image/jpeg,cover-image
)

REM For each chapter folder
cd /d %~dp0
cd /d "___tmp/EPUB"
set /a chapterNum=1
if !_DEBUGMODE!==0 (
for /d %%D in ("content/chapter*") do (
	
	cd /d %~dp0
	cd /d "___tmp/EPUB/content/%%D"
	set filename=../../package.opf

	echo [93m%%D[0m
	echo [93m--------------------------------------------------------------------------------[0m


	REM For each item in this chapter.
	for /F "delims=" %%A in ('dir /s/b /a:-d "*.*"') do (
		set B=%%A
		REM File extension.
		set ext=%%~xA
		REM File id
		set /a index+=1
		REM File href (relative to location of package.opf)

		set href=!B:%CD%\=!
		set type=ERROR
		set done=0

		REM ECHO TODO prevent the opf itself from being included in its own items.
		REM ECHO TODO add the 3 unique items to the package.opf
		REM ECHO TODO populate the opf with data from the xml
		REM ECHO Resolve the remaining significant epub_check errors.

		REM CALL :wubba !filename!,!index!,!href!,"D"

		REM https://stackoverflow.com/questions/32460177/how-can-i-find-out-a-files-mime-typecontent-type-on-windows
		REM Skip Thumbs.db files.
		REM NOTE:
		REM file.exe can be quite slow, and returns incorrect MIME types for certain files.
		REM for such cases, the types have been hardcoded.
		REM DO NOT try to trick it (aka, do not rename *.png files to *.txt for no reason. Have the file extension reflect the file type).

		REM content/chapterX/section1/index.html
		if "!href!"=="content\%%D\section1\index.html" if !done!==0 (
			call :writePackageItem !filename!,index-!chapterNum!,"!href!",text/html,scripted
			set /a index-=1
			set done=1
		)

		REM Image files
		if "!ext!"==".jpg" if !done!==0 (
			call :writePackageItem !filename!,!index!,"!href!",image/jpeg
			set done=1
		)
		if "!ext!"==".jpeg" if !done!==0 (
			call :writePackageItem !filename!,!index!,"!href!",image/jpeg
			set done=1
		)
		if "!ext!"==".png" if !done!==0 (
			call :writePackageItem !filename!,!index!,"!href!",image/png
			set done=1
		)
		if "!ext!"==".svg" if !done!==0 (
			call :writePackageItem !filename!,!index!,"!href!",image/svg+xml
			set done=1
		)
		if "!ext!"==".gif" if !done!==0 (
			call :writePackageItem !filename!,!index!,"!href!",image/gif
			set done=1
		)

		REM Scripts
		if "!ext!"==".js" if !done!==0 (
			call :writePackageItem !filename!,!index!,"!href!",text/javascript
			set done=1
		)
		if "!ext!"==".json" if !done!==0 (
			call :writePackageItem !filename!,!index!,"!href!",application/json
			set done=1
		)
		if "!ext!"==".html" if !done!==0 (
			call :writePackageItem !filename!,!index!,"!href!",text/html
			set done=1
		)
		if "!ext!"==".css" if !done!==0 (
			call :writePackageItem !filename!,!index!,"!href!",text/css
			set done=1
		)
		if "!ext!"==".less" if !done!==0 (
			call :writePackageItem !filename!,!index!,"!href!",text/plain
			set done=1
		)
		if "!ext!"==".xml" if !done!==0 (
			call :writePackageItem !filename!,!index!,"!href!",text/xml
			set done=1
		)
		if "!ext!"==".xhtml" if !done!==0 (
			call :writePackageItem !filename!,!index!,"!href!",application/xhtml+xml
			set done=1
		) 

		REM Use the (slower, but more accurate) file.exe if the file type is still unknown.
		REM And ignore all *.db files (Thumbs.db created by Windows 10).
		if NOT "!ext!"==".db" if !done!==0 (
			for /f "tokens=*" %%i in ('..\..\..\..\lib\file\bin\file --mime-type -b "!B!"') do (
				set type=%%i
				call :writePackageItem !filename!,!index!,"!href!",!type!,FILEEXE
			)
		)
	)

	set /a chapterNum+=1
	echo.
)
)

cd /d %~dp0
cd /d "___tmp/EPUB"
set filename=package.opf

ECHO 	^</manifest^> >> !filename!
ECHO 	^<spine^> >> !filename!

REM Create an entry for each chapters index.html file
REM ----------------------------------------------------
set /a chapterNum=1
for /d %%D in ("content/chapter*") do (
	ECHO 		^<itemref idref="id_index-!chapterNum!"/^> >> !filename!
	set /a chapterNum+=1
)

ECHO 	^</spine^> >> !filename!
ECHO ^</package^> >> !filename!


REM Generate the toc.xhtml file
REM ----------------------------------------------------
echo [93mCreating the toc.xhtml file...[0m
echo.

echo [93mTopic names:[0m

set filename=toc.xhtml

ECHO ^<?xml version="1.0" encoding="UTF-8" standalone="no"?^> > !filename!
ECHO ^<^^!DOCTYPE html^> >> !filename!
ECHO ^<html xmlns="http://www.w3.org/1999/xhtml" xmlns:epub="http://www.idpf.org/2007/ops" xml:lang="en" lang="en"^> >> !filename!
ECHO	^<head^> >> !filename!
ECHO		^<title^>!unitname!^</title^> >> !filename!
ECHO	^</head^> >> !filename!
ECHO	^<body^> >> !filename!
ECHO		^<h1^>!unitname!^</h1^> >> !filename!
ECHO		^<nav epub:type="toc" id="toc" role="doc-toc"^> >> !filename!
ECHO			^<h2^>Table of Contents^</h2^> >> !filename!
ECHO			^<ol^> >> !filename!

REM Create an entry for each chapters index.html file
REM ----------------------------------------------------
set /a chapterNum=1
for /d %%D in ("content/chapter*") do (
	for /f "tokens=* delims=" %%# in ('..\..\lib\xpath\xpath.bat "content/chapter!chapterNum!/section1/unitinfo.xml" "//topicname"') do set "topicname=%%#"
	ECHO				^<li^>^<a href="content/chapter!chapterNum!/section1/index.html"^>!topicname!^</a^>^</li^> >> !filename!
	echo [96m!chapterNum!: !topicname![0m
	set /a chapterNum+=1
)

ECHO			^</ol^> >> !filename!
ECHO		^</nav^> >> !filename!
ECHO	^</body^> >> !filename!
ECHO ^</html^> >> !filename!

echo.
echo [93mDone.[0m
echo.

if !_DEBUGMODE!==0 (
	
	REM Create a Zip of the ___tmp folder.
	REM ----------------------------------------------------
	REM Do NOT include Thumbs.db files in the .zip.
	echo [93mCreating the .epub using the ___tmp folder...[0m
	cd /D ../
	REM 7z a -r testZip.zip * -x^^!Thumbs.db
	REM 7z a testZip.zip -x^^!Thumbs.db

	7z a -r EPUB.zip * m=Deflate -x^^!Thumbs.db
	REM Rename the .zip to .epub
	REM (important to do this AFTER it's been created as a .zip. Speeds up Bookshelf loading by ~100x).
	rename "EPUB.zip" "!unitname!.epub"

	REM Move the newly created EPUB into the SCORM folder with the other .zips.
	move "!unitname!.epub" "../SCORM"
)

echo [93mDone.[0m
echo.


pause

REM Remove the temporary directory.
cd /D ../
if !_DEBUGMODE!==0 (
	if exist "___tmp" rmdir /s /q "___tmp" >nul
)

EXIT /B 0
EndLocal
ECHO ON

REM FUNCTIONS
REM ----------------------------------------------------

:writePackageItem
	setlocal enableextensions enabledelayedexpansion

	set "filename=%~1"
	set "index=%~2"
	set "href=%~3"
	set "type=%~4"
	set "properties=%~5"

	REM Do a different colored ECHO if this was calculated with file.exe (purely for debug info as file.exe is slower).
	set fileexe=0
	
	REM properties parameter is optional
	if defined properties (
		if "!properties!"=="FILEEXE" (
			set properties=
			set fileexe=1
		) else (
			set properties=properties="!properties!"
		) 
	) else (
		set properties=
	)

	REM Replace all backslashes with frontslashes in the URL.
	set "href=!href:\=/!"

	ECHO 		^<item id="id_!index!" href="!href!" media-type="!type!" !properties!/^> >> !filename!
	
	if !fileexe!==0 (
		ECHO !index! --- !href! --- !type!
	) else (
		ECHO [96m!index! --- !href! --- !type![0m
	)

	endlocal
	EXIT /B 0
REM ----------------------------------------------------