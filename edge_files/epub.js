(function(global, factory){
    if (typeof define === "function" && define.amd) define(factory);
    else if (typeof module === "object") module.exports = factory();
    else global.myModule = factory();
}(this, function(){
    "use strict";
    return function() {

		/**
		 * Batch script sets these values automatically when creating the topic:
		 * ---------------------------------------------------------------------
		 * unitName
		 * topicName
		 * ---------------------------------------------------------------------
		 * DO NOT remove or modify the comment below IN ANY WAY!
		 */
//BATCH//
		
		function init() {

			// Create a wrapper for the #Stage and the banner.
			let wrapperEl = document.createElement('div');
			wrapperEl.classList = "epub";

			let innerEl = document.createElement('div');
			innerEl.classList = "epub__inner";

			// Wraps the stage.
			let contentEl = document.createElement('div');
			contentEl.classList = "epub__content";

			let stageEl = document.getElementById("Stage");
			stageEl.parentNode.appendChild(wrapperEl);

			// Create the banner
			let bannerEl = document.createElement('div'); bannerEl.classList = "epub__banner";
			let bannerTitleEl = document.createElement('h1'); bannerTitleEl.classList = "epub__banner-title";
			let bannerSubTitleEl = document.createElement('h2'); bannerSubTitleEl.classList = "epub__banner-subtitle";
			let bannerImgEl = document.createElement("img"); bannerImgEl.classList = "epub__banner-img";
			bannerImgEl.setAttribute("src", "../banner-logo.svg");
			bannerImgEl.setAttribute("height", "68");
			bannerImgEl.setAttribute("width", "200");
			bannerImgEl.setAttribute("alt", "Didasko logo");

			bannerTitleEl.innerHTML = typeof unitName !== 'undefined' ? unitName : "ERROR";
			bannerSubTitleEl.innerHTML = typeof topicName !== 'undefined' ? topicName : "ERROR";

			bannerEl.appendChild(bannerTitleEl);
			bannerEl.appendChild(bannerSubTitleEl);
			bannerEl.appendChild(bannerImgEl);
			
			wrapperEl.appendChild(innerEl);
			innerEl.appendChild(bannerEl);
			innerEl.appendChild(contentEl);
			contentEl.appendChild(stageEl);

			//stageEl.style.position = "absolute !important";

			/*<div style="background-color: #0c3857; padding: 20px 30px; font-family: Arial, Helvetica, sans-serif; width: 1200px; height: 102px; box-sizing: border-box;">
            <h1 style="font-size: 26px; margin: 0; color: white; font-weight: normal;">UNIT_NAME</h1>
            <h2 style="font-size: 18px; margin: 0; margin-top: 10px; color: #42c4f1; font-weight: normal;">TOPIC_NAME</h2>
        </div>*/
		}

		window.onload = init;
	}
}));

new myModule();