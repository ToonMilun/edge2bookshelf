Warning
-------
- These are BATCH files. Do NOT modify them unless you know what you are doing!
- Likewise, do not attempt to stress or interact with the scripts without being prompted (while they are executing one of their processes). Unless you're Milton, don't interact with your computer until the batch program prompts you / is complete.
- The mimetype file MUST be the first file in the archive! Bookshelf will reject it if it isn't. Try this: https://superuser.com/questions/385392/how-can-i-control-the-order-of-files-in-a-zip-archive

Usage
-----
- These programs MUST be used from a LOCAL (C:) drive. If this is currently on the R: drive, be sure to copy the entire "edge2bookshelf" folder to C: first.
- Ensure that this folder ("edge2bookshelf") does NOT have a "___tmp" folder in it. If it does, delete it. The "___tmp" folder is a temporary folder made by the batch scripts. If it's still present when they're not running, then something has gone wrong.
- Place an entire unit's worth of Edge/Adapt (don't mix the two) SCORM .zips into the "SCORM" folder. Ensure there's nothing else in there besides them and the README.txt.
- Run either edge2bookshelf.bat or adapt2bookshelf.bat and follow the prompts.
- Your epub file will be placed inside the SCORM folder once it's made. You can now move it and rename it to wherever/whatever you need.

Troubleshooting
---------------
If you get errors relating to "Magic" it means that file.exe (in lib) is looking for an environmental variable.
You may need to do (in CMD):
set MAGIC=YOUR_DIRECTORY_HERE\edge2bookshelf\lib\file\bin
If all else fails, install file.exe instead (http://gnuwin32.sourceforge.net/packages/file.htm).
(file.exe is what is used to determine the mime type of each file in the manifest).



https://support.vitalsource.com/hc/en-us/articles/360015707993-Metadata-FAQ
https://support.vitalsource.com/hc/en-us/articles/360015708413-Metadata-Template-Requirements


epub_check fix notes
--------------------
- The fragment errors are being caused by the # in the URLs of the TOC... well what can I do about that though? Those are mandatory. One way to fix is to have the package.opf NOT reference the index.html. Then the errors stop? But then the EPUB stops working too. So the toc.xhtml and the package.opf are connecting the two somehow, but they're connecting them wrong?

- A lot of Adapt EPUB issues can be resolved by re-saving the media component's .svgs, and updating the broken CSS references in it.

- Aaaaaaand swapping over from HTML to XHTML for Adapt gets rid of all the fragment bugs

- The dynamic HTML to XHTML converter I made for Edge WORKS! The page does seem to chug a bit whenever the pagebreaks are repositioned, but outside of that it seeeeems fine.

- The YT flickering is caused only when the answer is incorrect. Interesting.
- Content also seems to jump around randomly after deliberately causing these flashes. Hm...
- Even if the content is in XHTML, the pagebreaks still need to be set up as soome as the HTML starts.


12/04/2021: list of files to update across all IT08 (Java) and BU11 topics:
+ grunt/tasks/epub.js
+ components/adapt-contrib-media
+ components/adapt-question-reorder/libraries/dd-reorder-draggable.js
+ core/less/reset.less
+ core/js/views/buttonsView.js
+ core/js/views/componentView.js
+ extensions/adapt-epub/js/adapt-epub.js
+ extensions/adapt-contrib-tutor/js/adapt-contrib-tutor.js
+ theme/adapt-didasko-theme/less/fonts.less
+ theme/adapt-didasko-theme/less/src/epub.less
+ theme/adapt-didasko-theme/less/src/libraries/jquery.mCustomScrollbar.less
+ DELETE: b-X/pic_X.png
+ DELETE: extensions/adapt-epub/required/index.html
+ COPY: extensions/adapt-epub/required/index.xhtml
+ COPY: extensions/adapt-epub/required/html2xhtml.xhtml
+ Move: src/core/assets/mCSB_buttons.png



































